# SC Public

Side-channel (power analysis, fault injection) notebooks for public access/publication.

## Legal

The code is published under MIT license (SPDX-License-Identifier: MIT), see [LICENSE](LICENSE).
The project has been cofounded by Polish National Centre for Research and Development (NCBR) under project "Evaluation of Side Channel Attack Potential on Embedded Targets (ESCAPE)", proj. sign. PL-TW/VII/5/2020.
