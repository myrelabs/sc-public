# Copyright 2020 - 2022, EMCORE Sp. z o.o.
# SPDX-License-Identifier: MIT

# patools must be in sys.path

from patools import ScopeTarget

def run_command(st: ScopeTarget, command, data=None):
    # Init CTR-DRBG
    if command == 'i':
        st.target.simpleserial_write('i', b'')
        st.target.simpleserial_wait_ack()
        return (0, None)
    # Seed CTR-DRBG with default entropy function
    elif command == 's':
        if data == None:
            return (-1, None)
        st.target.simpleserial_write('s', data)
        st.target.simpleserial_wait_ack()
        return (0, None)
    # Clean up CTR-DRBG context
    elif command == 'f':
        st.target.simpleserial_write('f', b'')
        st.target.simpleserial_wait_ack()
        return (0, None)
    # Dump seed (= key || nonce)
    elif command == 'x':
        st.target.simpleserial_write('x', b'')
        out = st.target.simpleserial_read('r', 48, timeout=5000)
        return (0, out)
    # Generate and read 1024 bits
    elif command == 'g':
        st.target.simpleserial_write('g', b'')
        st.target.simpleserial_wait_ack()
        chunks = 1024 // 128
        out = []
        for i in range(chunks):
            st.target.simpleserial_write('r', i.to_bytes(1, 'little'))
            out.append(st.target.simpleserial_read('r', 128, timeout=5000))
        return (0, out)
    # Set seed (= key || nonce) to custom value
    elif command == 'y':
        if data == None:
            return (-1, None)
        st.target.simpleserial_write('y', data)
        st.target.simpleserial_wait_ack()
        return 0
    # Measure generation of 1024 bits
    elif command == 'measure':
        st.scope.arm()
        st.target.simpleserial_write('g', b'')
        st.target.simpleserial_wait_ack()
        ret = st.scope.capture()
        if ret:
            print('Timeout happened during acquisition')
            return(ret, None)
        trace = st.scope.get_last_trace()
        return(0, trace)
    # Measure generation and read 1024 bits
    elif command == 'measure_g':
        st.scope.arm()
        st.target.simpleserial_write('g', b'')
        st.target.simpleserial_wait_ack()
        ret = st.scope.capture()
        if ret:
            print('Timeout happened during acquisition')
            return(ret, None)
        trace = st.scope.get_last_trace()
        chunks = 1024 // 128
        out = []
        for i in range(chunks):
            st.target.simpleserial_write('r', i.to_bytes(1, 'little'))
            out.append(st.target.simpleserial_read('r', 128, timeout=5000))
        return(0, trace, out)
    else:
        return (-2, None)
