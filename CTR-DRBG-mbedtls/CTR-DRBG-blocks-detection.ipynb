{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## AES blocks detection for CTR-DRBG power trace\n",
    "\n",
    "ChipWhisperer sample buffer can capture ~24k samples per trace. With x4 sample-to-clock ratio and our implementation, it is possible to capture only 3 full AES calls. The maximum number of CTR-DRBG blocks that can be requested with default configuration of mbedTLS library is 64 (this value can be changed in mbedTLS's *config.h*), while the attack referred to in **CTR-DRBG-DeMeyer2019.ipynb** uses up to 256 blocks of a single CTR-DRBG call.\n",
    "\n",
    "Instead of capturing one long CTR-DRBG execution, it can be called block-by-block, with seed beeing incremented manually (for this reason, our implementation allows for custom seed selection). While it likely cannot be done with real-life implementations, we prove that the trace captured \"in chunks\" is not far off from the potential traces captured in one call.\n",
    "\n",
    "This file describes an experiment when a trace of 3 blocks of AES in CTR-DRBG is captured in one call, then cut into 3 single AES traces and aligned. Next, 3 traces of AES in CTR-DRBG are captured with seed beeing manually incremented after every call. The traces obtained in both ways are compared visually. Since they overlap sufficiently, the second capture method (\"in chunks\") is a valiable replacement for capturing the whole multi-block trace.\n",
    "\n",
    "---\n",
    "\n",
    "This is a suplementary file for **CTR-DRBG-DeMeyer2019.ipynb**\n",
    "\n",
    "---\n",
    "\n",
    "Attacked program can be found on *myre-develop* branch of [Myre Laboratories' ChipWhisperer repository fork](https://gitlab.com/myrelabs/chipwhisperer/), in */hardware/victims/firmware/simpleserial-ctr-drbg* directory. For test, .hex file was compiled with *arm-none-eabi-gcc 9.2.1 20191025*, with -Os optimization flag."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "\n",
    "import chipwhisperer as cw\n",
    "from scipy.stats import pearsonr\n",
    "\n",
    "sys.path.append(\"../pa-tools\")\n",
    "\n",
    "from patools import ScopeTarget\n",
    "from patools.utils.plotting import bplotme\n",
    "import CTR_DRBG_common as drbg"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "st = ScopeTarget((), (cw.targets.SimpleSerial,))\n",
    "\n",
    "st.scope.default_setup()\n",
    "st.scope.adc.samples = 24000\n",
    "st.scope.adc.offset = 0\n",
    "st.scope.clock.adc_src = \"clkgen_x4\"\n",
    "st.set_clock()\n",
    "st.reset_target()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fw_path = f'./victims/simpleserial-ctr-drbg-CW308_STM32F3.hex'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set is_flashing_needed to True if you want to flash a hex file.\n",
    "# This is disabled by default to reduce target flash wear by unnecessary flashes.\n",
    "is_flashing_needed = True\n",
    "if is_flashing_needed:\n",
    "    st.reset_clock()\n",
    "    prog = cw.programmers.STM32FProgrammer\n",
    "    cw.program_target(st.scope, prog, fw_path)\n",
    "    st.set_clock()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def run_command(*args):\n",
    "    return drbg.run_command(st, *args)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "st.reset_target()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Step 1: Capture 24k samples of CTR-DRBG trace\n",
    "\n",
    "Three iterations (AES calls) should be distinguishable on the trace.\n",
    "\n",
    "In our case, the captured trace looks like this:\n",
    "\n",
    "<div>\n",
    "<img src=\"files/images/ctr_drbg_24k_trace.png\" title=\"24k samples of CTR-DRBG trace\" alt=\"24k samples of CTR-DRBG trace\" width=\"300\"/>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run_command('f')\n",
    "run_command('i')\n",
    "run_command('s', b'abcdabcd')\n",
    "same_seed = run_command('x')[1]\n",
    "(_, trace) = run_command('measure')\n",
    "bplotme(trace)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Step 2: Find and align iterations (separate AES calls) in the trace\n",
    "\n",
    "We use Pearson correlation coefficient to determine where the iterations start with exhaustive search in the given range; we choose alignment offset such that the absolute value of the coefficient of the previous iteration and processed iteration is the largest.\n",
    "\n",
    "As a result, we obtain three well-aligned iterations (each in different color):\n",
    "\n",
    "<div>\n",
    "<img src=\"files/images/ctr_drbg_aligned_iterations.png\" title=\"Aligned CTR-DRBG iterations\" alt=\"Aligned CTR-DRBG iterations\" width=\"300\"/>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def find_iterations(trace, iter_len, search_offset, num):\n",
    "    ts = [None for i in range(num)]\n",
    "    ts[0] = trace[0:iter_len]\n",
    "    max_corr = 0\n",
    "    prev_end = iter_len\n",
    "    for n in range(1, num):\n",
    "        new_end = prev_end\n",
    "        for i in range(search_offset * -1, search_offset + 1):\n",
    "            t_start = prev_end + i\n",
    "            t_end = t_start + iter_len\n",
    "            candidate = trace[t_start:t_end]\n",
    "            (corr, _) = pearsonr(ts[0], candidate)\n",
    "            if abs(corr) > abs(max_corr):\n",
    "                max_corr = corr\n",
    "                ts[n] = candidate\n",
    "                new_end = t_end\n",
    "        prev_end = new_end\n",
    "        max_corr = 0\n",
    "    return ts\n",
    "\n",
    "ts_split = find_iterations(trace, 7400, 500, 3)\n",
    "bplotme(ts_split[0], ts_split[1], ts_split[2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Step 3: Capture three traces of CTR-DRBG with seed incremented after every trace\n",
    "\n",
    "We call CTR-DRBG 3 times with *seed*, *seed+1* and *seed+2*, capturing the traces. Then, we visually compare captured traces with the corresponding fragments of the 3-block trace.\n",
    "\n",
    "We obtain almost perfect alignment (with exception to the beginning of the trace (which is expected)):\n",
    "\n",
    "<div>\n",
    "<img src=\"files/images/ctr_drbg_iter_1.png\" title=\"Comparison of the first iteration\" alt=\"Comparison of the first iteration\" width=\"300\"/>\n",
    "<img src=\"files/images/ctr_drbg_iter_2.png\" title=\"Comparison of the second iteration\" alt=\"Comparison of the second iteration\" width=\"300\"/>\n",
    "<img src=\"files/images/ctr_drbg_iter_3.png\" title=\"Comparison of the third iteration\" alt=\"Comparison of the third iteration\" width=\"300\"/>\n",
    "</div>\n",
    "\n",
    "Thus, our method is a valiable replacement for capturing long CTR-DRBG traces.\n",
    "\n",
    "##### Side note\n",
    "The comparison between different iterations shows visibly larger differences:\n",
    "\n",
    "<div>\n",
    "<img src=\"files/images/ctr_drbg_wrong_iter.png\" title=\"Comparison of different iterations\" alt=\"Comparison of different iterations\" width=\"300\"/>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def inc_seed(seed):\n",
    "    seed_int = int.from_bytes(seed[32:48], 'big')\n",
    "    seed_int += 1\n",
    "    new_seed = seed_int.to_bytes(16, 'big')    \n",
    "    tmp_seed = seed.copy()\n",
    "    tmp_seed[32:48] = new_seed\n",
    "    return tmp_seed\n",
    "\n",
    "def sim_trace_cutout(seed):\n",
    "    run_command('f')\n",
    "    run_command('i')\n",
    "    run_command('s', b'abcdabcd')\n",
    "    run_command('y', seed)\n",
    "    (_, t) = run_command('measure')\n",
    "    new_seed = inc_seed(seed)\n",
    "    return (new_seed, t)\n",
    "\n",
    "tr = []\n",
    "seed = same_seed\n",
    "for i in range(64):\n",
    "    (seed, t) = sim_trace_cutout(seed)\n",
    "    tr.append(t[0:7400])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bplotme(ts_split[0], tr[0])\n",
    "bplotme(ts_split[1], tr[1])\n",
    "bplotme(ts_split[2], tr[2])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# cleanup the connection to the target and scope\n",
    "st.close()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Legal\n",
    "\n",
    "The code is published under MIT license (SPDX-License-Identifier: MIT), see [LICENSE](../LICENSE).\n",
    "The project has been cofounded by Polish National Centre for Research and Development (NCBR) under project \"Evaluation of Side Channel Attack Potential on Embedded Targets (ESCAPE)\", proj. sign. PL-TW/VII/5/2020."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "vscode": {
   "interpreter": {
    "hash": "ec13b33d7c27dd4ea377f0feb28c386c9b496b2a1406318fb53cf506254431ca"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
