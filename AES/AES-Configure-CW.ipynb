{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "25c9c85e-9ea9-439e-950e-b16493361c6d",
   "metadata": {},
   "source": [
    "# AES Trace Gathering using ChipWhisperer hardware\n",
    "\n",
    "Attacked programs source code comes from *myre-develop* branch of [Myre Laboratories' ChipWhisperer repository fork](https://gitlab.com/myrelabs/chipwhisperer/), with main files in */hardware/victims/firmware/simpleserial-aes* directory. We focused on three AES implementations: TinyAES128C, MbedTLS and WolfSSL. For the configuration details, refer to the source code.\n",
    "\n",
    "All .hex files have been compiled with *arm-none-eabi-gcc 9.2.1 20191025*, with -Os optimization flag and can be found in *AES/victims* directory of this repository.\n",
    "\n",
    "This notebook lets you prepare and configure the gathering process without actually gathering traces; the gathering process has been extracted to [AES-Gather.ipynb](AES-Gather.ipynb) notebook, since it's common regardless of target or acquisition method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "051cf793-0498-4bc9-b064-ccc4086ac63e",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import sys\n",
    "\n",
    "import chipwhisperer as cw\n",
    "\n",
    "sys.path.append(\"../pa-tools\")\n",
    "\n",
    "from patools import ScopeTarget\n",
    "from patools.utils.misc import randbytes\n",
    "from patools.utils.plotting import bplotme"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "398b0443-9027-4dbb-86fd-9cb72cbf99de",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set target platform (may be used in FW path, will be added to trace metadata)\n",
    "target_platform = \"CW308_STM32F3\"\n",
    "# target_platform = \"CW308_K82F\"\n",
    "\n",
    "# Select AES implementation, typical: ['tinyAES128C'|'mbedTLS'|'wolfSSL'|'SecAES']\n",
    "target_impl = 'mbedTLS'\n",
    "# target_impl = 'mmCAU'  # CW308_K82F only\n",
    "\n",
    "# Select encryption variant ['enc'|'dec']\n",
    "variant = 'enc'\n",
    "\n",
    "# Set target_dir to a decent storage space (make sure that the directory exists!)\n",
    "target_dir = os.path.expanduser(f'~/traces/{target_platform}/{target_impl}')\n",
    "\n",
    "# Set firmware path to the target hex file (required only for flashing via CW)\n",
    "fw_path = f'./victims/simpleserial-aes-{variant}-{target_platform}-{target_impl}.hex'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aaf27943-4fed-43eb-8c0b-951cbe4e1ee7",
   "metadata": {},
   "source": [
    "# Scope & Target connection & settings\n",
    "\n",
    "Determine scope parameters manually, tune with the test cell below.\n",
    "The default values assume direct measurement of SHUNTL using direct ChipWhisperer connection to the line.\n",
    "Trace offsets and samples assume default target files. Feel free to add or modify"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "01358eb9-5c6d-48c1-9e53-9b0eb868fe37",
   "metadata": {},
   "outputs": [],
   "source": [
    "st = ScopeTarget((), (cw.targets.SimpleSerial,))\n",
    "\n",
    "st.target_metadata['algorithm'] = 'AES-128'\n",
    "st.target_metadata['platform'] = target_platform\n",
    "st.target_metadata['implementation'] = target_impl"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "163622e2-8f28-4c2c-82ae-1762698c2daa",
   "metadata": {},
   "outputs": [],
   "source": [
    "st.target_clock = int(24e6)\n",
    "\n",
    "st.scope.default_setup()\n",
    "st.scope.gain.gain = 30\n",
    "\n",
    "_confif = (target_platform, target_impl)\n",
    "\n",
    "if ('CW308_STM32F3', 'tinyAES128C') == _confif:\n",
    "    st.scope.adc.samples = 900\n",
    "    st.scope.adc.offset = 1200\n",
    "    \n",
    "elif ('CW308_STM32F3', 'mbedTLS') == _confif:\n",
    "    st.scope.adc.samples = 600\n",
    "    st.scope.adc.offset = 200\n",
    "    \n",
    "elif ('CW308_STM32F3', 'wolfSSL') == _confif:\n",
    "    st.scope.adc.samples = 400\n",
    "    st.scope.adc.offset = 300\n",
    "\n",
    "elif ('CW308_K82F', 'mmCAU') == _confif:\n",
    "    st.scope.adc.samples = 1664\n",
    "    st.scope.adc.offset = 0\n",
    "    # Gain has been selected for a pre-adjusted differential probe; YMMV\n",
    "    st.scope.gain.setGain(8)\n",
    "\n",
    "else:\n",
    "    print('Unknown target implementation selected')\n",
    "    st.scope.adc.samples = 24400\n",
    "    st.scope.adc.offset = 0\n",
    "\n",
    "st.scope.clock.adc_src = \"clkgen_x4\"\n",
    "st.set_clock()\n",
    "st.reset_target()\n",
    "\n",
    "# Common output file prefix\n",
    "target_prefix = f'{target_dir}/aes-{variant}-{st.fname_infix}'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6834e27f-d225-4b2e-a128-0818dcf04c02",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set is_flashing_needed to True if you want to flash a hex file.\n",
    "# This is disabled by default to reduce target flash wear by unnecessary flashes.\n",
    "is_flashing_needed = True\n",
    "if is_flashing_needed:\n",
    "    st.reset_clock()\n",
    "    prog = cw.programmers.STM32FProgrammer\n",
    "    cw.program_target(st.scope, prog, fw_path)\n",
    "    st.set_clock()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20e7477e-681d-40a9-a4fd-285ee8e975ae",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Test run\n",
    "key = bytes.fromhex('000102030405060708090a0b0c0d0e0f')\n",
    "txi = bytes.fromhex('00112233445566778899aabbccddeeff')\n",
    "exp = bytes.fromhex(\"69c4e0d86a7b0430d8cdb78070b4c55a\")\n",
    "if variant == 'dec':\n",
    "    txi, exp = exp, txi\n",
    "st.reset_target()\n",
    "\n",
    "# Uncomment the following for masked targets\n",
    "# st.target.simpleserial_write('m', b'\\x10' + randbytes(16))\n",
    "# assert st.target.simpleserial_wait_ack() is not None\n",
    "\n",
    "(trc, _, _, txo) = st.run_one(key, txi)\n",
    "bplotme(trc, title=\"Sample Trace\")\n",
    "print(\"Key:\", \" \".join(f\"{b:02x}\" for b in key))\n",
    "print(\"Txi:\", \" \".join(f\"{b:02x}\" for b in txi))\n",
    "print(\"Txo:\", \" \".join(f\"{b:02x}\" for b in txo))\n",
    "print(\"Full trigger count:\", st.scope.adc.trig_count)\n",
    "assert txo == exp"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bb0bd0d7-4320-45e1-935d-1120507070b3",
   "metadata": {},
   "source": [
    "# Gathering\n",
    "\n",
    "Trace gathering has been moved to [AES-Gather.ipynb](AES-Gather.ipynb) notebook; see there for further instructions."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "05e0ebe9",
   "metadata": {},
   "source": [
    "### Legal\n",
    "\n",
    "The code is published under MIT license (SPDX-License-Identifier: MIT), see [LICENSE](../LICENSE).\n",
    "The project has been cofounded by Polish National Centre for Research and Development (NCBR) under project \"Evaluation of Side Channel Attack Potential on Embedded Targets (ESCAPE)\", proj. sign. PL-TW/VII/5/2020."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "vscode": {
   "interpreter": {
    "hash": "8f1648d958707da8a3a775dba51ceaee8f239f699781cb3c6a02127f08f6fdcc"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
