{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "2625cc52",
   "metadata": {},
   "source": [
    "# CPA & DPA attacks on STM32F2-CRYP coprocessor\n",
    "\n",
    "The coprocessor leaks on internal state update. However, since a single round 1 state byte depends on 5 key bytes (4 in 0th ARK and 1 in 1st ARK), a straightforward attack is not feasible. Instead, carefully chosen inputs can be used to drastically reduce complexity of the attack.\n",
    "\n",
    "The traces must be gathered in such a way that after first round ShiftRows there are three constant bytes and one changing byte in each column (\\*). This allows to correlate between the intermediate states after AddRoundKey operations in the first and the second round, factoring out three bytes of first round MixColumns and AddRoundKey byte as an unknown constant.\n",
    "\n",
    "\\* A single changing column is also fine, but requires 4 times as many traces (16 sets instead of 4).\n",
    "\n",
    "## Input / Key / State mapping before and after shift rows\n",
    "\n",
    "```\n",
    "+--+--+--+--+         +--+--+--+--+\n",
    "| 0| 4| 8|12|         | 0| 4| 8|12|\n",
    "+--+--+--+--+         +--+--+--+--+\n",
    "| 1| 5| 9|13|         | 5| 9|13| 1|\n",
    "+--+--+--+--+   -->   +--+--+--+--+\n",
    "| 2| 6|10|14|         |10|14| 2| 6|\n",
    "+--+--+--+--+         +--+--+--+--+\n",
    "| 3| 7|11|15|         |15| 3| 7|11|\n",
    "+--+--+--+--+         +--+--+--+--+\n",
    "```\n",
    "\n",
    "Therefore columns contain the following input/key bytes:\n",
    " * Column 0 : 0, 5, 10, 15 (05AF)\n",
    " * Column 1 : 4, 9, 14, 3  (49E3)\n",
    " * Column 2 : 8, 13, 2, 7  (8D27)\n",
    " * Column 3 : 12, 1, 6, 11 (C16B)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "513a03d1",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import pickle\n",
    "import sys\n",
    "\n",
    "from tqdm.notebook import tqdm, trange\n",
    "\n",
    "sys.path.append(\"../pa-tools\")\n",
    "\n",
    "from patools import CorrelationPowerAnalysis as CPA, \\\n",
    "                    DifferentialPowerAnalysis as DPA, \\\n",
    "                    TracesFileProxy\n",
    "from patools.traces import precompute_difftraces, align_fragments\n",
    "from patools.utils.misc import hw\n",
    "from patools.utils.plotting import bplotme\n",
    "from patools.victims.aes_tools import get_mixcol_comp, shiftrow_state"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4b5e79c0",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set quiet to True to silence the most verbose logs\n",
    "quiet = False"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8a185176",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set target_dir to a path containing the attack traces\n",
    "target_dir = os.path.expanduser(\"~/traces/CW308_STM32F2/HWAES\")\n",
    "# The target_prefix should be common for all byte-targeting traces\n",
    "target_prefix = f\"{target_dir}/aes-MSO64BScope-24000000-0-10-diffB\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "45767470",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8f0ede6a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define target window (select on traces) and alignment window\n",
    "# 8GS, 5MHz, STM32F2\n",
    "target_window  = (9000, 9700)  # The points of the source traces to be correlated (after alignment) - best to focus on a single clock cycle with small margin\n",
    "align_segment  = (75, 175)     # Select distinct clock cycle feature similar on different inputs, used to align traces by the feature; signifies points within the target window\n",
    "shift_range    = (-64, 64)     # Maximum shift range for the alignment algorithm\n",
    "target_segment = (0, 500)      # The points within target_window used for correlation\n",
    "# 8GS, 10MHz, STM32F2\n",
    "# target_window  = (4900, 5700)  # The points of the source traces to be correlated (after alignment) - best to focus on a single clock cycle with small margin\n",
    "# align_segment  = (200, 300)    # Select distinct clock cycle feature similar on different inputs, used to align traces by the feature; signifies points within the target window\n",
    "# shift_range    = (-64, 64)     # Maximum shift range for the alignment algorithm\n",
    "# target_segment = (180, 600)    # The points within target_window used for correlation\n",
    "# 25GS, 24MHz, STM32F4\n",
    "# target_window  = (5000, 6000)\n",
    "# align_segment  = (200, 600)\n",
    "# shift_range    = (-200, 200)\n",
    "# target_segment = (380, 420)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2fc874fe",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Reset best guesses, helpful for store/reload incomplete analysis\n",
    "bg_0 = bg_1 = bg_2 = bg_3 = bg_4 = bg_5 = bg_6 = bg_7 = bg_8 = bg_9 = bg_A = bg_B = bg_C = bg_D = bg_E = bg_F = []"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "edb183b7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load and pre-align attack traces\n",
    "tracefs = [\n",
    "    TracesFileProxy.load(f\"{target_prefix}-col0-4k\"),\n",
    "    TracesFileProxy.load(f\"{target_prefix}-col1-4k\"),\n",
    "    TracesFileProxy.load(f\"{target_prefix}-col2-4k\"),\n",
    "    TracesFileProxy.load(f\"{target_prefix}-col3-4k\"),\n",
    "]\n",
    "aligned_cols = [\n",
    "    align_fragments(tracef.traces, target_window, align_segment, shift_range)\n",
    "    for tracef in tracefs\n",
    "]"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "90950e36",
   "metadata": {},
   "source": [
    "## Base attack (per-byte CPA)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6488601d",
   "metadata": {},
   "outputs": [],
   "source": [
    "def perform_correlation(traces, textins, subkey):\n",
    "    up_subkey = shiftrow_state(subkey)\n",
    "        \n",
    "    def hw_func(guess, txi):\n",
    "        keyb, mixcb = guess\n",
    "        inter = get_mixcol_comp(keyb, txi[subkey], 3)\n",
    "        state = mixcb ^ inter\n",
    "        return hw(state ^ txi[up_subkey] ^ keyb)\n",
    "\n",
    "    meantrace, difftraces, nvar_trace = precompute_difftraces(traces[:,target_segment[0]:target_segment[1]])\n",
    "    bplotme(meantrace)\n",
    "    # The first 128 cases of mixcb yield the same absolute results (but with different sign before abs())\n",
    "    # as their bitflips, so we can limit the search space to 128 only\n",
    "    guess_gen = tqdm(((keyb, mixcb) for keyb in range(256) for mixcb in range(128)), total=256*128)\n",
    "    ris_sorted = CPA.single(hw_func, difftraces, nvar_trace, textins, guess_gen)\n",
    "    if not quiet:\n",
    "        for (keyb, mixcb), corr in ris_sorted[:32]:\n",
    "            print(f\"Guess {keyb:02x} {mixcb:02x} - {corr}\")\n",
    "    \n",
    "    return ris_sorted\n",
    "\n",
    "def perform_attack(subkey):\n",
    "    return perform_correlation(aligned_cols[subkey // 4], tracefs[subkey // 4].textins, subkey)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "fed34827",
   "metadata": {},
   "source": [
    "### Column 0123"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bea28764",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_0 = perform_attack(0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "78371f8c",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_1 = perform_attack(1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2530395a",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_2 = perform_attack(2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cec48857",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_3 = perform_attack(3)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "7b20fd72",
   "metadata": {},
   "source": [
    "### Column 4567"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8de7e92f",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_4 = perform_attack(4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ba435ace",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_5 = perform_attack(5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fd74759f",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_6 = perform_attack(6)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "af064261",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_7 = perform_attack(7)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "f0637a30",
   "metadata": {},
   "source": [
    "### Column 89AB"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3df42061",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_8 = perform_attack(8)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1b49a1c9",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_9 = perform_attack(9)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2071abf7",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_A = perform_attack(10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "840f8ba6",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_B = perform_attack(11)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "8ffd2df0",
   "metadata": {},
   "source": [
    "### Column CDEF"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f4f12537",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_C = perform_attack(12)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0c4e7165",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_D = perform_attack(13)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3664ba01",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_E = perform_attack(14)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5e654f3f",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_F = perform_attack(15)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "a328c2f6",
   "metadata": {},
   "source": [
    "### Save / reload best per-byte guess lists"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e0e01aa1",
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(\"best-guesses.pic\", \"wb\") as f:\n",
    "    pickle.dump((bg_0, bg_1, bg_2, bg_3, \n",
    "                 bg_4, bg_5, bg_6, bg_7, \n",
    "                 bg_8, bg_9, bg_A, bg_B, \n",
    "                 bg_C, bg_D, bg_E, bg_F), f)\n",
    "with open(\"best-guesses.txt\", \"w\") as f:\n",
    "    for i, bg in enumerate((bg_0, bg_1, bg_2, bg_3, \n",
    "                            bg_4, bg_5, bg_6, bg_7, \n",
    "                            bg_8, bg_9, bg_A, bg_B, \n",
    "                            bg_C, bg_D, bg_E, bg_F)):\n",
    "        f.write(f\"Key byte {i}\\n\")\n",
    "        for line in bg:\n",
    "            f.write(f\"{line}\\n\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b35e040e",
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(\"best-guesses.pic\", \"rb\") as f:\n",
    "    (bg_0, bg_1, bg_2, bg_3, \n",
    "     bg_4, bg_5, bg_6, bg_7, \n",
    "     bg_8, bg_9, bg_A, bg_B, \n",
    "     bg_C, bg_D, bg_E, bg_F) = pickle.load(f)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ccd7f3be",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "7c893ff8",
   "metadata": {},
   "source": [
    "# Alternative attack (per-byte DPA)\n",
    "Unlike CPA, DPA can still be performed in 2^8 complexity, because the constant factor of mixcb only flips the bucket order, but results in the same overall trace partition (just like when DPA cannot be performed prior to SubBytes, because no matter the key guess, trace partition will always be the same)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2420290a",
   "metadata": {},
   "outputs": [],
   "source": [
    "def perform_correlation(traces, textins, subkey):\n",
    "    tfragment = traces[:,target_segment[0]:target_segment[1]]\n",
    "    up_subkey = shiftrow_state(subkey)\n",
    "    \n",
    "    nbits = 8\n",
    "    def bits(x, n=nbits):\n",
    "        return tuple((x >> s) & 1 for s in range(n))\n",
    "    \n",
    "    def sel_func(guess, txi):\n",
    "        keyb = guess\n",
    "        inter = get_mixcol_comp(keyb, txi[subkey], 3)\n",
    "        return bits(inter ^ txi[up_subkey])\n",
    "\n",
    "    guess_gen = trange(256)\n",
    "    ris_sorted = DPA.single(sel_func, tfragment, textins, guess_gen, nbits)\n",
    "    if not quiet:\n",
    "        for keyb, corr in ris_sorted[:32]:\n",
    "            print(f\"Guess {keyb:02x} - {corr}\")\n",
    "    \n",
    "    return ris_sorted \n",
    "\n",
    "def perform_attack(subkey):\n",
    "    return perform_correlation(aligned_cols[subkey // 4], tracefs[subkey // 4].textins, subkey)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "01f7dcf9",
   "metadata": {},
   "source": [
    "### Column 0123"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "93c40fc6",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_0 = perform_attack(0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c9081494",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_1 = perform_attack(1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e9ee1673",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_2 = perform_attack(2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "885b835f",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_3 = perform_attack(3)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "267d057d",
   "metadata": {},
   "source": [
    "### Column 4567"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c76440c5",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_4 = perform_attack(4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "39de50a8",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_5 = perform_attack(5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "293651ca",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_6 = perform_attack(6)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "427707c7",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_7 = perform_attack(7)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "3230570b",
   "metadata": {},
   "source": [
    "### Column 89AB"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "82dc7d6c",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_8 = perform_attack(8)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d8ae5438",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_9 = perform_attack(9)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7c4162f0",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_A = perform_attack(10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9012593e",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_B = perform_attack(11)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "135ebe62",
   "metadata": {},
   "source": [
    "### Column CDEF"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a295baca",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_C = perform_attack(12)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "33ed88bb",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_D = perform_attack(13)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8d64f714",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_E = perform_attack(14)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1353e13b",
   "metadata": {},
   "outputs": [],
   "source": [
    "bg_F = perform_attack(15)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "4f7570cf",
   "metadata": {},
   "source": [
    "### Save / reload best per-byte guess lists"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a459568b",
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(\"best-guesses.pic\", \"wb\") as f:\n",
    "    pickle.dump((bg_0, bg_1, bg_2, bg_3, \n",
    "                 bg_4, bg_5, bg_6, bg_7, \n",
    "                 bg_8, bg_9, bg_A, bg_B, \n",
    "                 bg_C, bg_D, bg_E, bg_F), f)\n",
    "with open(\"best-guesses.txt\", \"w\") as f:\n",
    "    for i, bg in enumerate((bg_0, bg_1, bg_2, bg_3, \n",
    "                            bg_4, bg_5, bg_6, bg_7, \n",
    "                            bg_8, bg_9, bg_A, bg_B, \n",
    "                            bg_C, bg_D, bg_E, bg_F)):\n",
    "        f.write(f\"Key byte {i}\\n\")\n",
    "        for line in bg:\n",
    "            f.write(f\"{line}\\n\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7d3fc8f1",
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(\"best-guesses.pic\", \"rb\") as f:\n",
    "    (bg_0, bg_1, bg_2, bg_3, \n",
    "     bg_4, bg_5, bg_6, bg_7, \n",
    "     bg_8, bg_9, bg_A, bg_B, \n",
    "     bg_C, bg_D, bg_E, bg_F) = pickle.load(f)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0df292c3",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "e097e16a",
   "metadata": {},
   "source": [
    "### Legal\n",
    "\n",
    "The code is published under MIT license (SPDX-License-Identifier: MIT), see [LICENSE](../LICENSE).\n",
    "The project has been cofounded by Polish National Centre for Research and Development (NCBR) under project \"Evaluation of Side Channel Attack Potential on Embedded Targets (ESCAPE)\", proj. sign. PL-TW/VII/5/2020."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "vscode": {
   "interpreter": {
    "hash": "ec13b33d7c27dd4ea377f0feb28c386c9b496b2a1406318fb53cf506254431ca"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
