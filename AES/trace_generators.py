# Copyright 2020 - 2022, EMCORE Sp. z o.o.
# SPDX-License-Identifier: MIT

# This file contains generators of (key, txi) pairs for trace gathering using
# `ScopeTarget` method `gather_for_generator`.

import random
from typing import Union
from chipwhisperer.analyzer.attacks.models.aes.key_schedule import key_schedule_rounds

from patools.victims.aes_tools import \
    rev_HW, sbox, rev_sbox, \
    rev_ft_HW_map, rev_rt_HW_map, \
    possible_ft_HW, possible_rt_HW
from patools.utils.misc import randbytes


# The challenge key to be used in challenge traces;
# set globally with :py_func:`~set_challenge_key`
challenge_key = bytearray(16)
# Whenever a new random key is to be generated,
# if reuse_random_keys is set to an integer `n` > 1,
# the same key will be reused in the next `n` traces;
# This is helpful when collecting traces using multigather,
# as all multigather traces need to have the same key
reuse_random_keys: Union[bool, int] = False


def set_challenge_key(key):
    """
    Set challenge key for challenge generators, e.g.:
     * gen_rand_row
     * gen_uni_txi
    """
    global challenge_key
    challenge_key[:] = key


def set_reuse_random_keys(value: Union[bool, int]):
    """
    Set whether random keys should be reused, c.f.
    `reuse_random_keys`.
    """
    global reuse_random_keys
    reuse_random_keys = value


def _random_key_generator():
    countdown = 0
    while True:
        if countdown == 0:
            key = randbytes(16)
            if reuse_random_keys is not False and reuse_random_keys > 1:
                countdown = reuse_random_keys - 1
        else:
            countdown -= 1
        yield key


def gen_cat(*generators):
    """
    Concatenate multiple generators.
    """
    for gen in generators:
        yield from gen


def gen_const(key, txi):
    def _gen(n):
        for _ in range(n):
            yield (key, txi)
    return _gen


def gen_uni_txi(n):
    """
    Generate `n` challenge traces with random input (txi) and constant key (challenge_key).

    Example use cases: naive CPA, DPA, TA (attack phase).

    :param n: number of pairs to generate
    :returns: **yields:** n (key, input) pairs
    """
    for _ in range(n):
        txi = randbytes(16)
        yield (challenge_key, txi)


def gen_uni_all(n):
    """
    Generate `n` traces with random input (txi) and keys.

    Example use cases: TA (training phase), trace correlation analysis

    :param n: number of pairs to generate
    :returns: **yields:** n (key, input) pairs
    """
    keygen = _random_key_generator()
    for _ in range(n):
        key = next(keygen)
        txi = randbytes(16)
        yield (key, txi)


def gen_rand_row(row, const):
    """
    Return a generator for challenge traces with constant key (challenge_key) and
    selectively random input (txi).

    For each input, selected `row` values are chosen at random, while the rest
    is kept constant (as in `const`). Since in AES data is interpreted column-first,
    this can be interpreted as:
     * set txi = const
     * replace every 4th byte (starting from `row`-th byte) with random.
    
    Example use cases: CPA/DPA (single row only).

    :param row: 
    :returns: **yields:** n (key, input) pairs
    """
    row_mask = 0x000000FF000000FF000000FF000000FF << (row * 8)
    const_int = int.from_bytes(const, 'little')

    def _gen(n):
        for _ in range(n):
            txi_int = random.getrandbits(128)
            txi_int = (const_int & ~row_mask) | (txi_int & row_mask)
            txi_bts = txi_int.to_bytes(16, 'little')
            yield (challenge_key, txi_bts)

    return _gen


def gen_rand_col(col, const):
    """
    Return a generator for challenge traces with constant key (challenge_key) and
    selectively random input (txi).

    For each input, selected `col` values are chosen at random, while the rest
    is kept constant (as in `const`). Since in AES data is interpreted column-first,
    this can be interpreted as:
     * set txi = const
     * replace bytes [4*row:4*row+4] with random.
    
    Example use cases: CPA/DPA (single row only).

    :param row: 
    :returns: **yields:** n (key, input) pairs
    """
    col_mask = 0x000000000000000000000000FFFFFFFF << (col * 32)
    const_int = int.from_bytes(const, 'little')

    def _gen(n):
        for _ in range(n):
            txi_int = random.getrandbits(128)
            txi_int = (const_int & ~col_mask) | (txi_int & col_mask)
            txi_bts = txi_int.to_bytes(16, 'little')
            yield (challenge_key, txi_bts)

    return _gen


def gen_uni_hw32_ark(n):
    """
    Generate `n` traces with uniform distribution of 32-bit hamming weights
    (single columns) of the first AddRoundKey results.
    Distriutions of `key` and `txi` should each appear uniform, but are highly correlated
    as values where `key == txi` are much more likely (1/33 instead of 1/2^31).

    Example use cases: TA on ARK result (training phase).

    :param n: number of pairs to generate
    :returns: **yields:** n (key, input) pairs
    """
    hw_templates = ['0' * (32 - i) + '1' * i for i in range(33)]
    keygen = _random_key_generator()
    for _ in range(n):
        key = next(keygen)
        key_int = int.from_bytes(key, 'little')
        ark_hws = [random.randrange(33) for _ in range(4)]
        ark_bin = ''.join(''.join(random.sample(hw_templates[ark_hw], 32))
                          for ark_hw in ark_hws)
        ark_int = int(ark_bin, 2)
        txi_int = ark_int ^ key_int
        txi = txi_int.to_bytes(16, 'little')
        yield (key, txi)


def gen_uni_hw32_rark(n):
    """
    Generate `n` traces with uniform distribution of 32-bit hamming weights
    (single columns) of the first AddRoundKey output in decrypt mode.
    Distriutions of `key` and `txi` should each appear uniform, but are highly correlated
    as values where `key == txi` are much more likely (1/33 instead of 1/2^31).

    Example use cases: TA on ARK result (training phase).

    :param n: number of pairs to generate
    :returns: **yields:** n (key, input) pairs
    """
    hw_templates = ['0' * (32 - i) + '1' * i for i in range(33)]
    keygen = _random_key_generator()
    for _ in range(n):
        rkey = next(keygen)
        rkey_int = int.from_bytes(rkey, 'little')
        ark_hws = [random.randrange(33) for _ in range(4)]
        ark_bin = ''.join(''.join(random.sample(hw_templates[ark_hw], 32))
                          for ark_hw in ark_hws)
        ark_int = int(ark_bin, 2)
        txi_int = ark_int ^ rkey_int
        txi = txi_int.to_bytes(16, 'little')
        key = bytes(key_schedule_rounds(rkey, 10, 0))
        yield (key, txi)


def get_txi_for_hw_8(hw, keyb):
    """
    Uniformly choose a random input byte such that
    the Hamming weight of the S-box output is as
    defined by the argument.

    :param hw: desired Hamming weight
    :param keyb: corresponding byte key
    :returns: plaintext byte (ptb) s.t. HW(sbox[ptb ^ keyb]) == hw
    """
    sb_out = random.choice(rev_HW[hw])
    xor = rev_sbox[sb_out]
    ptb = keyb ^ xor
    return ptb


def get_txi_for_hw_32(hw, keyb):
    """
    Uniformly choose a random input byte such that
    the Hamming weight of the Forward Table (FT) output is as
    defined by the argument.

    :param hw: desired Hamming weight
    :param keyb: corresponding byte key
    :returns: plaintext byte (ptb) s.t. HW(FT[ptb ^ keyb]) == hw
    """
    xor = random.choice(rev_ft_HW_map[hw])
    ptb = keyb ^ xor
    return ptb


def get_txi_for_hw_8r(hw, keyb):
    """
    Uniformly choose a random input byte such that
    the Hamming weight of the reverse S-box output is as
    defined by the argument.

    :param hw: desired Hamming weight
    :param keyb: corresponding byte key
    :returns: ciphertext byte (ctb) s.t. HW(rev_sbox[ctb ^ keyb]) == hw
    """
    sb_out = random.choice(rev_HW[hw])
    xor = sbox[sb_out]
    ctb = keyb ^ xor
    return ctb


def get_txi_for_hw_32r(hw, keyb):
    """
    Uniformly choose a random input byte such that
    the Hamming weight of the Reverse Table (FT) output is as
    defined by the argument.

    :param hw: desired Hamming weight
    :param keyb: corresponding byte key
    :returns: ciphertext byte (ctb) s.t. HW(RT[ctb ^ keyb]) == hw
    """
    xor = random.choice(rev_rt_HW_map[hw])
    ctb = keyb ^ xor
    return ctb


def gen_uni_hw8_sb(n):
    """
    Generate `n` traces with uniform distribution of 8-bit hamming weights
    (single bytes) of the first SubBytes results.

    :param n: number of pairs to generate
    :returns: **yields:** n (key, input) pairs
    """
    keygen = _random_key_generator()
    for _ in range(n):
        key = next(keygen)
        txi = bytes(
            get_txi_for_hw_8(random.randint(0, 8), keyb) for keyb in key)
        yield (key, txi)


def gen_uni_hw32_sb(n):
    """
    Generate `n` traces such that the Hamming weights of their bytewise
    Forward Table (FT) outputs are evenly distributed.

    :param n: number of pairs to generate
    :returns: **yields:** n (key, input) pairs
    """
    keygen = _random_key_generator()
    for _ in range(n):
        key = next(keygen)
        txi = bytes(
            get_txi_for_hw_32(random.choice(possible_ft_HW), keyb)
            for keyb in key)
        yield (key, txi)


def gen_uni_hw8_rsb(n):
    """
    Generate `n` traces with uniform distribution of 8-bit hamming weights
    (single bytes) of the first InvSubBytes results.

    :param n: number of pairs to generate
    :returns: **yields:** n (key, input) pairs
    """
    keygen = _random_key_generator()
    for _ in range(n):
        rkey = next(keygen)
        txi = bytes(
            get_txi_for_hw_8r(random.randint(0, 8), keyb) for keyb in rkey)
        key = bytes(key_schedule_rounds(rkey, 10, 0))
        yield (key, txi)


def gen_uni_hw32_rsb(n):
    """
    Generate `n` traces such that the Hamming weights of their bytewise
    Reverse Table (RT) outputs are evenly distributed.

    :param n: number of pairs to generate
    :returns: **yields:** n (key, input) pairs
    """
    keygen = _random_key_generator()
    for _ in range(n):
        rkey = next(keygen)
        txi = bytes(
            get_txi_for_hw_32r(random.choice(possible_rt_HW), keyb)
            for keyb in rkey)
        key = bytes(key_schedule_rounds(rkey, 10, 0))
        yield (key, txi)
