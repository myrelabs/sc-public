# Copyright 2020 - 2022, EMCORE Sp. z o.o.
# SPDX-License-Identifier: MIT

using Mmap
using ProgressBars

#=
Compute probability of an event occurring `events` times with `trials` attempts
when the probability of a single event occurring is `event_prob`.
=#
binomial_prob(events, trials, event_prob) = binomial(BigInt(trials), events) *
                                            event_prob^events *
                                            (1 - event_prob)^(trials - events)

# Compute only a denominator part of `binomial_prob`.
binomial_prob_den(        trials, event_prob) = denominator(event_prob)^trials

# Compute only a numerator part of `binomial_prob`.
binomial_prob_num(events, trials, event_prob) = binomial(BigInt(trials), events) *
                                                numerator(event_prob)^events *
                                                (denominator(event_prob) - numerator(event_prob))^(trials-events)

#=
Compute probability of a particular single bucket overflowing when `balls_num`
balls are thrown uniformly into the buckets, there are `buckets` buckets
in total and each bucket has `capacity` capacity.
=#
function single_prob(buckets, capacity, balls_num)
  event_prob = Rational{BigInt}(1 // buckets)
  overflow_prob_den = binomial_prob_den(balls_num, event_prob)
  overflow_prob_num = overflow_prob_den
  for balls_in_bucket in 0:min(capacity, balls_num)
    overflow_prob_num -= binomial_prob_num(balls_in_bucket, balls_num, event_prob)
  end
  return Float64(overflow_prob_num / overflow_prob_den)
end

#=
Compute upper bound of the probability that any bucket overflows when `balls`
number of balls are thrown uniformly into the buckets, there are `buckets`
number of buckets in total and each bucket has `capacity` capacity. The upper
bound is determined with the first step of the inclusion-exclusion principle
(only the first inclusion; yields the exact result when `balls < capacity + 1`).
=#
prob_upper_bound(buckets, capacity, balls) = Float64(single_prob(buckets, capacity, balls) * buckets)

#=
Optimized version of `prob_upper_bound` with slightly different interface.
Again, `buckets` is the number of buckets and `capacity`
is a single bucket capacity. The approximation is run for each number of balls
of `ball_range` range values. The result is stored in the array `probs`.
The function displays a progress bar on `balls_range`.
=#
function prob_upper_bound_fill(probs, buckets, capacity, ball_range)
  event_prob_num = 1
  event_prob_den = buckets

  overflow_prob_num = 0
  overflow_prob_den = 0
  first_loop = true

  for balls_num in ProgressBar(ball_range)
    if first_loop
      # Skip the cases where there are not enough balls to overflow a bucket.
      if balls_num <= capacity
        probs[balls_num + 1] = 0
        continue
      end
      #=
      Optimization: Overflow probability denominator is calculated
      incrementally; the values for the iterations are subsequent powers
      of `event_prob_den`, so there is no need to compute it from scratch
      for each iteration.
      =#
      overflow_prob_den_prev = BigInt(event_prob_den) ^ (balls_num-1)
      overflow_prob_den = overflow_prob_den_prev * event_prob_den
      first_loop = false
    else
      overflow_prob_den_prev = overflow_prob_den
      overflow_prob_den *= event_prob_den
    end

    #=
    Initial overflow probability is 1. Then, the probabilities of all possible
    outcomes such that overflow DOES NOT occur are subtracted
    (when exactly 0, 1, ..., or `capacity` balls end up in the bucket).
    This way, the loop iterates at most `capacity + 1` number of times.
    =#
    overflow_prob_num = overflow_prob_den

    #=
    Overflow does not occur when there are at most `capacity` balls in a bucket.
    Also, avoid putting non-existing balls into the bucket when capacity
    is larger than the total number of balls.
    =#
    max_bib = min(capacity, balls_num)

    #=
    In each loop iteration, subtract probability that `balls_in_bucket` number
    of balls fall into the bucket; it is done using binomial distribution:
    `overflow_prob_num -=
            binomial(balls_num, balls_in_bucket) * success_comp * failure_comp`.
    Note that only numerator is computed, denominator has already been
    determined at the beginning of the outer loop iteration.
    Since `event_prob_num == 1`, `success_comp` is always 1 and can be omitted.
    Other values can be computed incrementally, as implemented in the code
    below.
    =#
    failure_fact = BigInt(event_prob_den - event_prob_num)
    failure_comp = failure_fact^(balls_num - max_bib)
    binomial_comp = binomial(BigInt(balls_num), max_bib)
    for balls_in_bucket in max_bib:-1:0
      overflow_prob_num -= binomial_comp * failure_comp
      binomial_comp *=  balls_in_bucket
      binomial_comp ÷= (balls_num - balls_in_bucket + 1)
      failure_comp *= failure_fact
    end

    #=
    Use `overflow_prob_den_prev` instead of `overflow_prob_den / buckets`,
    because `buckets == event_prob_den`
    and `overflow_prob_den_prev * event_prob_den == overflow_prob_den`.
    =#
    current_prob = Float64(overflow_prob_num / overflow_prob_den_prev)
    probs[balls_num + 1] = current_prob
  end
end

#=
Compute probability of both buckets in a particular pair of buckets overflowing
when `balls_num` balls are thrown uniformly into the buckets, there are
`buckets` buckets in total and each bucket has `capacity` capacity.
=#
function pairs_prob(buckets, capacity, balls_num)
  total_prob = 0
  place_in_2_prob = Rational{BigInt}(2 // buckets)
  half_prob = Rational{BigInt}(1 // 2)
  for total in 2*capacity+2:balls_num
    prob_equal = binomial_prob(total, balls_num, place_in_2_prob)
    if prob_equal == 0
      break
    end
    small_sum = 0
    for balls_in_one in capacity+1:total-capacity-1
      small_sum += binomial_prob(balls_in_one, total, half_prob)
    end
    total_prob += Float64(prob_equal * small_sum)
  end
  return total_prob
end

#=
Compute probability that two particular buckets of `buckets` number of buckets
overflow when `balls` number of balls is thrown into the buckets uniformly
at random and multiply the obtained probability by `pairs` - the number
of possible buckets pairs. Each bucket has the same capacity equal to
`capacity`. The value obtained as a result of this function is the second step
in the inclusion-exclusion principle implementation - it is the value
of the first exclusion. Subtracting the value of the first exclusion
from the value of the first inclusion yields the lower bound of the probability
that at least one bucket overflows in the setup described above.
(`The first inclusion - The first exclusion` yields the exact result
when `balls < 2(capacity + 1)`.)
=#
function prob_lower_bound(buckets, capacity, balls, pairs)
  fp = pairs_prob(buckets, capacity, balls)
  return fp * pairs
end

#=
Optimized version of `prob_lower_bound` with slightly different interface.
Again, `buckets` is the number of buckets and `capacity` is a single bucket
capacity. The approximation is run for each number of balls of `ball_range`
range values. The result is stored in the array `probs_lo`.
The function displays a progress bar on `balls_range`.
=#
function prob_lower_bound_fill(probs_lo::Array{Float64,1}, buckets, capacity, ball_range)
  pairs = BigInt(buckets * (buckets - 1) ÷ 2) # binomial(BigInt(buckets), 2)

  max_ball_range = last(ball_range)

  #=
  Precompute initial value of the binomial component needed for latter
  calculations of probability that after `total` number of balls land
  in two buckets, both buckets overflow. Precomputed value assumes that
  all the balls land in two buckets, so `total == balls_num` (this value is
  later adjusted iteratively when `total` decrements with the loop).
  The goal is to overflow two buckets at once and the probability is
  calculated as a sum of probabilities of events such that `balls_in_one` balls
  land in one bucket and the rest land in another one. Initially,
  `balls_in_one` is equal to `total - capacity - 1` (maximal number of balls
  in one bucket such that the other one still overflows). Thus, binomial part
  of probability formula in this setup is the following:
  `binomial(balls_num, balls_num - capacity - 1)`.

  Here, we iterate over `balls_num`, starting calculation when it is greater
  than capacity, so the first iteration value is
  `binomial((capacity + 1), (capacity + 1) - capacity - 1) = 1`.

  This part is sequentially precomputed here because its results need to be
  available at the same time later on when the function switches
  to multithreading and when `ball_range` skips some iterations.
  =#
  small_binomial_comp_prev_iter = BigInt(1)
  small_binomial_comp_inits = Array{BigFloat, 1}(undef, max_ball_range)
  # 0 can be safely skipped as `0 < 2 * capacity + 2` for any capacity.
  for balls_num in ProgressBar(1:max_ball_range)
    if balls_num <= capacity
      small_binomial_comp_inits[balls_num] = 0
    else
      small_binomial_comp_inits[balls_num] = BigFloat(small_binomial_comp_prev_iter)
      #=
      Iterative calculation of `binomial(balls_num, balls_num - capacity - 1)`;
      Variable `balls_num` increments with each iteration, so the next value
      can be calculated using the following property:
      `binomial(a + 1, b + 1) = binomial(a, b) * (a + 1) / (b + 1)`.
      =#
      small_binomial_comp_prev_iter = small_binomial_comp_prev_iter * (balls_num + 1) ÷ (balls_num - capacity)
    end
  end

  pb = ProgressBar(ball_range)
  l = ReentrantLock()
  next = iterate(pb)

  nthreads = Threads.nthreads()
  Threads.@threads for threadid in 1:nthreads
    lock(l)
    while next !== nothing
      balls_num::Integer = 0
      try
        balls_num, state = next
        next = iterate(pb, state)
      finally
        unlock(l)
      end

      if balls_num >= 2*capacity+2
        total_prob = 0

        #=
        Iteratively calculate a numerator of the probability that
        out of `balls_num` balls, exactly `total` number of balls land in two
        specific buckets. The probability is represented by the formula
        `binomial(balls_num, total) * (2 / buckets)^total * (1 - 2 / buckets)^(balls_num - total)`.
        The first component is called `binomial_comp` in the code below.
        The second component is `success_comp` and it is not stored in any
        variable; since we compute only numerator, it is always a power of 2,
        so multiplication can be replaced by shift operation.
        The third component is `failure_comp`. Note that
        `1 - 2 / buckets = (buckets - 2) / buckets`.
        We calculate only numerator since `binomial_comp` is always an integer
        and denominator part of `failure_comp * succes_comp` can be computed
        once for all iterations - it is stored as `prob_equal_den` in the code.
        =#
        prob_equal_den = BigFloat(buckets)^balls_num

        failure_fact = buckets - 2
        failure_comp = BigInt(1)   # failure_fact^(balls_num - balls_num)
        binomial_comp = BigInt(1)  # binomial(balls_num, total=balls_num)

        # Extract precomputed initial binomial component of the probability.
        small_binomial_comp_init = small_binomial_comp_inits[balls_num]

        # Stop when `total` balls are not enough to overflow both buckets.
        for total in balls_num:-1:2*capacity+2
          # Probability equals `binomial_comp * failure_comp * success_comp`.
          # Float64 has not enough range here.
          prob_equal_num = BigFloat((binomial_comp * failure_comp) << total)

          # Because `binomial(b, t - 1) = (t / (b - t + 1)) * binomial(b, t)`.
          binomial_comp *=  total
          binomial_comp ÷= (balls_num - total + 1)
          failure_comp *= failure_fact

          #=
          Calculate probability that after `total` number of balls land
          in two buckets, both buckets overflow. It is computed as a sum
          of probabilities of events such that `balls_in_one` balls land
          in one bucket and the rest land in another one for all values of
          `balls_in_one` such that both buckets overflow.
          =#
          small_sum_num = Float64(0)
          small_binomial_comp = small_binomial_comp_init

          #=
          Adjust for next iteration when total decrements. The value of
          the initial binomial comp is `binomial(total, total - capacity - 1)`.
          Use the property: `binomial(a - 1, b - 1) = binomial(a, b) * b / a`.
          =#
          small_binomial_comp_init *= (total - capacity - 1)
          small_binomial_comp_init ÷=  total

          #=
          Since probability of a single ball landing in one of two buckets
          is 1/2, the complementary event has the same probability.
          The probability of exactly `m` of `n` balls falling into the first
          bucket is `binomial(n, m) * (1/2)^m * (1 - 1/2)^(n-m)`, which equals
          `binomial(n, m) * (1/2)^n`. Thus, only the binomial component needs
          to be computed when `m` changes; the rest is constant.
          Since we want to compute a sum of such probabilities for all values
          of `m`, we can simply multiply the constant with the sum
          of resulting binomials.
          =#
          for balls_in_one in total-capacity-1:-1:capacity+1
            small_sum_num += small_binomial_comp
            # Because `binomial(n, m - 1) = binomial(n, m) * m / (n - m + 1)`.
            small_binomial_comp *=  balls_in_one
            small_binomial_comp ÷= (total - balls_in_one + 1)
          end
          #=
          Add probability of both buckets overflowing when `total` balls fall
          into these two buckets to the aggregator for probability that
          both buckets overflow when there are `balls_num` balls thrown
          in the experiment.
          =#
          total_prob += prob_equal_num * small_sum_num * (0.5^total)
        end
        #=
        Include precomputed denominator. Multiply by the number of possible
        pairs of buckets (needed in the second step of inclusion-exclusion).
        =#
        fp = Float64(total_prob / prob_equal_den) * pairs
      else
        fp = 0
      end

      probs_lo[balls_num + 1] = fp
      lock(l)
    end
  end
end

#=
Open mmapped file named `fname` to store calculated results.
Create the directory and a file itself if they don't exist.
=#
function open_probs_mmap(fname, ball_range)
  dname = "overflow_probs"
  ffname = string(dname, "/", fname, ".bin")

  mkpath("overflow_probs")

  if !isfile(ffname)
    f = open(ffname, "w+")
    Mmap.mmap(f, Vector{Float64}, (last(ball_range) + 1,))
    close(f)
  end

  f = open(ffname, "r+")
  probs = Mmap.mmap(f, Vector{Float64}, (last(ball_range) + 1,))
  return f, probs

end

#=
Calculate and store in a file the upper bound of the probability that
when `b` balls (for `b` in `ball_range`) are put into `buckets` buckets
of capacity `capacity`, some bucket overflows. Optimized version.
The results DO NOT cap at 1 (it needs to be cut manually).
=#
function probs_upper_bounds_fill(buckets, capacity, ball_range)
  f, probs = open_probs_mmap("upper_bounds", ball_range)
  prob_upper_bound_fill(probs, buckets, capacity, ball_range)
  close(f)
end

#=
Calculate and store in a file the upper bound of the probability that
when `b` balls (for `b` in `ball_range`) are put into `buckets` buckets
of capacity `capacity`, some bucket overflows. Unoptimized version.
The results DO NOT cap at 1 (it needs to be cut manually).
=#
function probs_upper_bounds(buckets, capacity, ball_range)
  f, probs = open_probs_mmap("upper_bounds", ball_range)

  Threads.@threads for k in ProgressBar(ball_range)
    probs[k + 1] = prob_upper_bound(buckets, capacity, k)
  end

  close(f)
end

#=
Calculate and store in a file the difference between the upper and the lower
bound (the value of the first exclusion in inclusion-exclusion principle
implementation) of the probability that when `b` balls (for `b` in `ball_range`)
are put into `buckets` buckets of capacity `capacity`, some bucket overflows.
Optimized version.
=#
function probs_lower_bounds_diff_fill(buckets, capacity, ball_range)
  flo, probs_lo = open_probs_mmap("lower_bounds_diff", ball_range)

  prob_lower_bound_fill(probs_lo,  buckets, capacity, ball_range)
  println(probs_lo[last(ball_range) + 1])

  close(flo)
end

#=
Calculate and store in a file the difference between the upper and the lower
bound (the value of the first exclusion in inclusion-exclusion principle
implementation) of the probability that when `b` balls (for `b` in `ball_range`)
are put into `buckets` buckets of capacity `capacity`, some bucket overflows.
Unoptimized version.
=#
function probs_lower_bounds_diff(buckets, capacity, ball_range)
  flo, probs_lo = open_probs_mmap("lower_bounds_diff", ball_range)

  pairs = binomial(BigInt(buckets), 2)
  Threads.@threads for k in ProgressBar(ball_range)
    probs_lo[k + 1] = prob_lower_bound(buckets, capacity, k, pairs)
  end
  println(probs_lo[last(ball_range) + 1])

  close(flo)
end

#=
Calculate and store in a file the probability lower bound based on the
probabilities calculated in probs_upper_bounds[,_fill] and
probs_lower_bounds_diff[,_fill].
The results DO NOT cap at 0 (it needs to be cut manually).
=#
function probs_lower_bounds(ball_range)
  flo, probs_lo = open_probs_mmap("lower_bounds", ball_range)

  fld = open("overflow_probs/lower_bounds_diff.bin", "r+")
  probs_lo_diff = Mmap.mmap(fld, Vector{Float64}, (last(ball_range) + 1,))

  fup = open("overflow_probs/upper_bounds.bin", "r+")
  probs_up = Mmap.mmap(fup, Vector{Float64}, (last(ball_range) + 1,))

  for k in ProgressBar(ball_range)
    probs_lo[k + 1] = probs_up[k + 1] - probs_lo_diff[k + 1]
  end

  close(fld)
  close(flo)
  close(fup)
end

# Data
buckets  = 256
capacity = 255

# Run
probs_upper_bounds_fill(buckets, capacity, 0:64000)
probs_lower_bounds_diff_fill(buckets, capacity, 0:50:64000)
probs_lower_bounds(0:50:64000)
