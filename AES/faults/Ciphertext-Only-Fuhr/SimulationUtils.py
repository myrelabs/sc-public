# Copyright 2020 - 2022, EMCORE Sp. z o.o.
# SPDX-License-Identifier: MIT

from chipwhisperer.analyzer import aes_funcs

def key_schedule(key):
    """
    Run AES-128 key schedule.

    :param key: master key
    :returns: list of round keys
    """
    return [aes_funcs.key_schedule_rounds(key, 0, i) for i in range(11)]

def do_round(subkey, state):
    """
    Compute one AES round.

    :param subkey: round key
    :param state: state after the previous round
    :returns: state after this round
    """
    return [k ^ s for (k, s) in zip(subkey, aes_funcs.mixcolumns(
                aes_funcs.shiftrows(
                    aes_funcs.subbytes(state.copy()))))]

def do_rounds(subkeys, txi, rounds):
    """
    Compute AES-128 up to after a specific round.

    :param subkeys: list of the round keys
    :param txi: 16-byte encryption input (plaintext)
    :param rounds: number of rounds to be computed
    :returns: state after a specific round
    """
    state = [k ^ s for (k, s) in zip(subkeys[0], txi)]
    for i in range(rounds):
        state = do_round(subkeys[i + 1], state)
    return state

def do_final(lastkey, state):
    """
    Compute AES final round.

    :param lastkey: key of the last round
    :param state: state before the final round
    :returns: encryption output (ciphertext)
    """
    return [k ^ s for (k, s) in zip(lastkey, aes_funcs.shiftrows(
                    aes_funcs.subbytes(state.copy())))]

def do_aes(subkeys, txi):
    """
    Compute AES-128.

    :param subkeys: list of the round keys
    :param txi: 16-byte encryption input (plaintext)
    :returns: encryption output (ciphertext)
    """
    return do_final(subkeys[10], do_rounds(subkeys, txi, 9))

# AES ShiftRow permutation
shift_row_lookup = [
    0, 13, 10, 7,
    4, 1, 14, 11,
    8, 5, 2, 15,
    12, 9, 6, 3
]