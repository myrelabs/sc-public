# Copyright 2020 - 2022, EMCORE Sp. z o.o.
# SPDX-License-Identifier: MIT

import itertools
import random

class Perturbation:
    """
    The class provides a capability of AND-perturbing a byte in an AES state
    with an error distribution specified in the constructor.
    
    :ivar state_prob: probability distribution of a faulted byte value
    """
    @staticmethod
    def get_state_prob(error_prob):
        """
        Compute a faulted byte distribution based on an error distribution.

        :param error_prob: error distribution
        :returns: faulted byte distribution
        """
        state_prob = [0] * 256
        for state in range(256):
            for error in range(256):
                state_prob[state & error] += 1/256 * error_prob[error]
        return state_prob

    @staticmethod
    def perturb_and(state, index, error):
        """
        Apply the AND-perturbation to a byte at a specified index.

        :param state: 16-byte bytes-like initial state
        :param index: index of a byte to be perturbed
        :param error: value of the error
        :returns: perturbed state
        """
        state = state.copy()
        state[index] &= error
        return state
    
    def perturb(self, state, index):
        """
        Apply the AND-perturbation to a byte at a specified index
        according to the error distribution.

        :param state: 16-byte bytes-like initial state
        :param index: index of a byte to be perturbed
        :returns: perturbed state
        """
        raise NotImplementedError

    def __init__(self, error_prob):
        self.state_prob = self.get_state_prob(error_prob)

class PerturbationStuckAt0(Perturbation):
    """
    The class provides a capability of perturbing a byte in an AES state
    such that a faulted byte always becomes 0.
    """
    def __init__(self):
        super().__init__([1.] + ([0.] * 255))
        
    def perturb(self, state, index):
        return self.perturb_and(state, index, 0)

class PerturbationStuckAt0Half(Perturbation):
    """
    The class provides a capability of AND-perturbing a byte in an AES state such that
    
    * with 50% probability, the value of the error is 0,
    * with 50% probability, the value of the error is chosen uniformly at random from a set 0..255.
    
    """
    def __init__(self):
        super().__init__([0.5 + 1 / 512] + ([1 / 512] * 255))
        
    def perturb(self, state, index):
        coin = random.choice([True, False])
        return self.perturb_and(state, index, 0 if coin else random.randrange(0, 256))

class PerturbationStuckAtRandom(Perturbation):
    """
    The class provides a capability of AND-perturbing a byte in an AES state such that
    the value of the error is chosen uniformly at random from a set 0..255.
    """
    def __init__(self):
        super().__init__([1 / 256] * 256)

    def perturb(self, state, index):
        return self.perturb_and(state, index, random.randrange(0, 256))

class RandomPerturbation(Perturbation):
    """
    The class provides a capability of perturbing a byte in an AES state such that
    the byte is assigned a random value according to a given distribution.
    If no distribution is given, an instance generates its own:
    all the byte values are assigned a random weight from 1 to 10.
    """
    def __init__(self, distribution=None):
        if distribution is None:
            distribution = [random.randrange(1, 11) for _ in range(256)]
        bulk_weight = sum(distribution)
        self.state_prob = [d / bulk_weight for d in distribution]
        self.cum_weights = list(itertools.accumulate(distribution))
        
    def perturb(self, state, index):
        state = state.copy()
        state[index] = random.choices([b for b in range(256)], cum_weights=self.cum_weights)[0]
        return state

class NonePerturbation(Perturbation):
    """
    The class provides a capability to use Perturbation instance
    without injecting any fault (for example, for debugging).
    """
    def __init__(self):
        super().__init__([0.] * 255 + [1.])

    def perturb(self, state, index):
        return state