# Copyright 2020 - 2022, EMCORE Sp. z o.o.
# SPDX-License-Identifier: MIT

import binascii
import dataclasses

@dataclasses.dataclass
class CtxSerializer:
    # If True, a correct ciphertext will be written to a file only once,
    # at the beginning of the file (or after the last round key, if present)
    # and faulted ciphertexts will be listed below it, one per line.
    # If False, correct-faulted ciphertexts will be put in pairs, one pair
    # per line, with space separating a correct ciphertext from a faulted one.
    use_single_plaintext: bool = False
    
    # If True, the last round key will be placed at the beginning of the file,
    # in a separate line.
    save_last_round_key: bool = False

    def save(self, filename, ciphertexts, key=None):
        with open(filename, 'w') as f:
            if self.save_last_round_key:
                if key is None:
                    raise Exception('No key to be saved')
                f.write(f'{binascii.hexlify(key).decode()}\n')
            if self.use_single_plaintext:
                f.write(f'{binascii.hexlify(ciphertexts[0][0]).decode()}\n')
            for c in ciphertexts:
                if not self.use_single_plaintext:
                    f.write(f'{binascii.hexlify(c[0]).decode()} ')
                f.write(f'{binascii.hexlify(c[1]).decode()}\n')

    def load(self, filename):
        with open(filename, 'r') as f:
            lines = f.readlines()
        key = None
        if self.save_last_round_key:
            key = binascii.unhexlify(lines[0].strip())
            lines = lines[1:]
        if self.use_single_plaintext:
            correct = binascii.unhexlify(lines[0].strip())
            lines = lines[1:]
        ciphertexts = []
        for l in lines:
            if len(l) == 0:
                continue
            if self.use_single_plaintext:
                ciphertexts.append((correct, binascii.unhexlify(l.strip())))
            else:
                correct, faulted = l.strip().split(' ')
                ciphertexts.append((binascii.unhexlify(correct),
                                    binascii.unhexlify(faulted)))

        return {'key': key if key else None, 'ciphertexts': ciphertexts}
