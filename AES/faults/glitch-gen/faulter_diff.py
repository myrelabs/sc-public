# Copyright 2020 - 2022, EMCORE Sp. z o.o.
# SPDX-License-Identifier: MIT

# A tool for visual analysis of a fault quality and consequences.
# It helps to determine when and where the fault has occured;
# given an AES encryption key, a plaintext and a faulted ciphertext,
# it allows to visually compare the outcome of a plaintext encryption
# and a faulted ciphertext decryption for each encryption/decryption step.

from chipwhisperer.analyzer import aes_funcs

# Compute all round keys from AES encryption key
def key_schedule(key):
    return [aes_funcs.key_schedule_rounds(key, 0, i) for i in range(15)]

# Define each encryption / decryption step (ChipWhisperer functions need
# wrappers since some of them modify input).
def add_key(key, text):
    return [k ^ t for (k, t) in zip(key, text)]

def sub_bytes(state):
    return aes_funcs.subbytes(state.copy())

def inv_sub_bytes(state):
    return aes_funcs.inv_subbytes(state.copy())

def shift_rows(state):
    return aes_funcs.shiftrows(state.copy())

def inv_shift_rows(state):
    return aes_funcs.inv_shiftrows(state.copy())

def mix_columns(state):
    return aes_funcs.mixcolumns(state.copy())

def inv_mix_columns(state):
    return aes_funcs.inv_mixcolumns(state.copy())

def format_tag(r, item):
    return(f'round[{r: >2}].{item: <9}')

# Compute all intermediate states of the encryption and store them in a list.
def compute_forward(rks, plaintext):
    states = []
    states.append((format_tag(0, 'ptx'), [b for b in plaintext]))
    states.append((format_tag(0, 'k_sch'), rks[0]))
    state = add_key(rks[0], plaintext)
    
    for r in range(1, 10):
        states.append((format_tag(r, 'start'), state))
        state = sub_bytes(state)
        states.append((format_tag(r, 's_box'), state))
        state = shift_rows(state)
        states.append((format_tag(r, 's_row'), state))
        state = mix_columns(state)
        states.append((format_tag(r, 'm_col'), state))
        states.append((format_tag(r, 'k_sch'), rks[r]))
        state = add_key(rks[r], state)
        
    states.append((format_tag(10, 'start'), state))
    state = sub_bytes(state)
    states.append((format_tag(10, 's_box'), state))
    state = shift_rows(state)
    states.append((format_tag(10, 's_row'), state))
    states.append((format_tag(10, 'k_sch'), rks[10]))
    state = add_key(rks[10], state)
    states.append((format_tag(10, 'ctx'), state))
    
    return states

# Compute all intermediate states of the decryption and store them in a list.
def compute_backward(rks, ciphertext):
    states = []
    states.append((format_tag(10, 'ctx'), [b for b in ciphertext]))
    states.append((format_tag(10, 'k_sch'), rks[10]))
    state = add_key(rks[10], ciphertext)
    states.append((format_tag(10, 's_row'), state))
    state = inv_shift_rows(state)
    states.append((format_tag(10, 's_box'), state))
    state = inv_sub_bytes(state)
    states.append((format_tag(10, 'start'), state))
    
    for r in range(9, 0, -1):
        states.append((format_tag(r, 'k_sch'), rks[r]))
        state = add_key(rks[r], state)
        states.append((format_tag(r, 'm_col'), state))
        state = inv_mix_columns(state)
        states.append((format_tag(r, 's_row'), state))
        state = inv_shift_rows(state)
        states.append((format_tag(r, 's_box'), state))
        state = inv_sub_bytes(state)
        states.append((format_tag(r, 'start'), state))
        
    states.append((format_tag(0, 'k_sch'), rks[0]))
    
    state = add_key(rks[0], state)
    states.append((format_tag(0, 'ptx'), state))

    return states

def print_compare_bytes(fw, bw, favor_bw):
    for byte_f, byte_b in zip(fw[1], bw[1]):
        if byte_f == byte_b:
            print(f'{byte_b:02x}', end = '')
        else:
            color, favored = (31, byte_b) if favor_bw else (32, byte_f)
            print(f'\u001b[{color}m{favored:02x}\u001b[0m', end = '')

# Visually compare encryption and decryption states.
# Diverging bytes are highlighted in red (backward calculation states)
# and green (forward calculation states).
def print_compare(states_fw, states_bw):
    states_bw = states_bw[::-1]
    for fw, bw in zip(states_fw, states_bw):
        if fw[0] != bw[0]:
            # Sanity check
            print('Error', fw[0], bw[0])
        print(fw[0], end = ' ')
        print_compare_bytes(fw, bw, True)
        print('    ', end='')
        print_compare_bytes(fw, bw, False)
        print()