{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# CPA attack on NXP/Freescale mmCAU coprocessor\n",
    "\n",
    "The coprocessor leaks on bus loads, primarily the result of the first AddRoundKey operation. See cells in the [Relevant Trace Analysis](#relevant-trace-analysis) section.\n",
    "\n",
    "The attack results in correlation for 5 values per row (4 bytes) and is augmented by \"xor-crunch-row\" algorithm to compute combined weight for all $2^{32}$ row candidates.\n",
    "This step is optional, but otherwise it's required to determine which of the 5 values (per row) is the most noisy and using the remaining 4 to extract key guess (the values are `k[i], k[i]^k[i+4], k[i+4]^k[i+8], k[i+8]^k[i+12], k[i+12]`)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import sys\n",
    "\n",
    "import numpy as np\n",
    "\n",
    "from tqdm.notebook import tqdm\n",
    "\n",
    "sys.path.append(\"../pa-tools\")\n",
    "\n",
    "from patools import CorrelationPowerAnalysis as CPA, CPAModel, TracesFileProxy\n",
    "from patools.traces import precompute_difftraces, corr_traces_pre\n",
    "from patools.utils.misc import hw, format_guesses, bytewise_xor as bxor\n",
    "from patools.utils.plotting import bplotme\n",
    "from patools.victims.aes_tools import sbox\n",
    "\n",
    "from chipwhisperer.analyzer import aes_funcs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set target_dir to a decent storage space\n",
    "target_dir = os.path.expanduser('~/traces')\n",
    "target_platform = 'CW308_K82F'\n",
    "# There are 2 hardware coprocessors, but this notebook is specific to mmCAU\n",
    "target_lib = 'mmCAU'\n",
    "# The target_prefix should be common for all byte-targeting traces\n",
    "target_prefix = f\"{target_dir}/{target_platform}/{target_lib}/aes-enc-24000000-0-1664-\""
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Required traces\n",
    "\n",
    "The notebook performs a CPA attack against column differences during loading first ARK results into mmCAU (see below). It requires prepared single row changes (*chosen input*) attack traces (see \"Single row changes\" in [AES-Gather.ipynb](AES-Gather.ipynb)), and any set of *known random key, known random input* traces for initial analysis (see \"Uniform key / input for correlation / template attacks\" in [AES-Gather.ipynb](AES-Gather.ipynb)). ChipWhisperer with differential probe is sufficient."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Relevant trace analysis\n",
    "\n",
    "For this attack, we observed that the first ARK leaks a lot as its performed on the CPU, but the remaining operations\n",
    "do not as they are performed on the mmCAU coprocessor (still possible that they leak, but not as much as the first xor).\n",
    "\n",
    "From mmcau_aes_functions.s (mmcau_lib_release/mmcau/asm-cm4/src/):\n",
    "```\n",
    "# prepare for AES operations register load\n",
    "    movw    r0, #:lower16:encrypt_reg_data\n",
    "    movt    r0, #:upper16:encrypt_reg_data\n",
    "\n",
    "# XOR the first 4 keys into the 16 plain text bytes\n",
    "    ldmia   r1!, {r8-fp}                            @ get key_sch[0-3]; r1++\n",
    "    eor     r4, r8\n",
    "    eor     r5, r9\n",
    "    eor     r6, sl\n",
    "    eor     r7, fp\n",
    "\n",
    "# load registers needed for mmcau commands from encrypt_reg_data:\n",
    "    ldmia   r0, {r0,r8-ip}                          @ setup AES operations\n",
    "\n",
    "# load the XOR results into the CAU's CA0 - CA3 registers\n",
    "    stmia   fp, {r4-r7}                             @ load CA0-CA3\n",
    "\n",
    "# send a series of cau commands to perform the encryption\n",
    "    str     r0, [ip]                                @ SubBytes\n",
    "    str     r8, [ip]                                @ SubBytes, ShiftRows\n",
    "    ldmia   r1!, {r4-r7}                            @ get next 4 keys; r1++\n",
    "    stmia   r9, {r4-r7}                             @ MixColumns\n",
    "```\n",
    "\n",
    "Most likely, the best target is the `load CA0-CA3` line (`stmia   fp, {r4-r7}`). Since this results in consecutive memory bus transfers, the bus first rises from neutral to `r4` and then switches to `r5, r6, r7` in consecutive cycles, falling back to neutral afterwards. Therefore we target `r4`, then `r4^r5`, then `r5^r6` and finally `r6^r7`. The last can be augmented with pure `r7`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "analysis_input_file = f\"{target_prefix}unihw32-ark-64k\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with TracesFileProxy.load(analysis_input_file) as f:\n",
    "    meantrace, difftraces, nvar_trace = precompute_difftraces(f.traces)\n",
    "    bplotme(meantrace)\n",
    "    analysis_keys = f.keys\n",
    "    analysis_textins = f.textins\n",
    "    analysis_textouts = f.textouts"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "round_keys = [\n",
    "    [\n",
    "        aes_funcs.key_schedule_rounds(key, 0, i)\n",
    "        for key in analysis_keys\n",
    "    ]\n",
    "    for i in range(11)\n",
    "]\n",
    "\n",
    "intermediates_addroundkey_0 = [\n",
    "    bytes(k ^ t for (k, t) in zip(key, txi))\n",
    "    for key, txi in zip(round_keys[0], analysis_textins)\n",
    "]\n",
    "intermediates_subbytes_0 = [\n",
    "    bytes(sbox[b] for b in state)\n",
    "    for state in intermediates_addroundkey_0\n",
    "]\n",
    "intermediates_shiftrows_0 = [\n",
    "    bytes(aes_funcs.shiftrows(bytearray(state)))\n",
    "    for state in intermediates_subbytes_0\n",
    "]\n",
    "intermediates_mixcolumns_0 = [\n",
    "    bytes(aes_funcs.mixcolumns(bytearray(state)))\n",
    "    for state in intermediates_shiftrows_0\n",
    "]\n",
    "\n",
    "intermediates_addroundkey_1 = [\n",
    "    bytes(k ^ t for (k, t) in zip(key, state))\n",
    "    for key, state in zip(round_keys[1], intermediates_mixcolumns_0)\n",
    "]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Input HW (32)\")\n",
    "corrs = []\n",
    "for i in range(4):\n",
    "    corrs.append(abs(corr_traces_pre([hw(txi[4*i:4*i+4]) for txi in f.textins], difftraces, nvar_trace)))\n",
    "bplotme(*corrs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Round 0 AddRoundKey HW (32)\")\n",
    "corrs = []\n",
    "for i in range(4):\n",
    "    corrs.append(abs(corr_traces_pre([hw(state[4*i:4*i+4]) for state in intermediates_addroundkey_0], difftraces, nvar_trace)))\n",
    "bplotme(*corrs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Round 0 SubBytes HW (32)\")\n",
    "corrs = []\n",
    "for i in range(4):\n",
    "    corrs.append(abs(corr_traces_pre([hw(state[4*i:4*i+4]) for state in intermediates_subbytes_0], difftraces, nvar_trace)))\n",
    "bplotme(*corrs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Output HW (32)\")\n",
    "corrs = []\n",
    "for i in range(4):\n",
    "    corrs.append(abs(corr_traces_pre([hw(txo[4*i:4*i+4]) for txo in f.textouts], difftraces, nvar_trace)))\n",
    "bplotme(*corrs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Round 0 AddRoundKey Inter-word update\")\n",
    "corrs = []\n",
    "for i in range(3):\n",
    "    corrs.append(\n",
    "        abs(\n",
    "            corr_traces_pre([\n",
    "                hw(bxor(state[4*i:4*i+4], state[4*i+4:4*i+8]))\n",
    "                for state in intermediates_addroundkey_0\n",
    "            ], difftraces, nvar_trace)))\n",
    "bplotme(*corrs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# CPA using single row changes\n",
    "\n",
    "In this attack, instead of using random traces to correlate, we use single-row-changes traces. Since we observe single column word (and then column ^ column) leaks, changing a single byte per column and keeping the rest constant, lets us observe better correlation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "attack_traces = [\n",
    "    TracesFileProxy.load(f'{target_prefix}row0-4k'),\n",
    "    TracesFileProxy.load(f'{target_prefix}row1-4k'),\n",
    "    TracesFileProxy.load(f'{target_prefix}row2-4k'),\n",
    "    TracesFileProxy.load(f'{target_prefix}row3-4k')\n",
    "]\n",
    "\n",
    "# Window should be a span containing all high-correlation points from\n",
    "# Round 0 AddRoundKey HW (32)\n",
    "# and\n",
    "# Round 0 AddRoundKey Inter-word update\n",
    "\n",
    "# NOTE: if attacking using a different scope than CW, this will require additional alignment\n",
    "\n",
    "attack_windows = [\n",
    "    precompute_difftraces(trc.traces[:,550:660]) for trc in attack_traces\n",
    "]\n",
    "\n",
    "# For hypothesis validation only:\n",
    "base_key = attack_traces[0].keys[0]\n",
    "\n",
    "wpkey = lambda x: x[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bg0a, bg1a, bg2a, bg3a = [\n",
    "    CPA.single((lambda guess, txi: hw(txi[i] ^ guess)), attack_windows[i][1], attack_windows[i][2], attack_traces[i].textins, range(256), model=CPAModel.POS)\n",
    "    for i in range(4)\n",
    "]\n",
    "format_guesses(base_key[:4], [bg0a, bg1a, bg2a, bg3a], weight_progression=True, weight_key=wpkey)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bg0b, bg1b, bg2b, bg3b = [\n",
    "    CPA.single((lambda guess, txi: hw(txi[i] ^ txi[4+i] ^ guess)), attack_windows[i][1], attack_windows[i][2], attack_traces[i].textins, range(256), model=CPAModel.POS)\n",
    "    for i in range(4)\n",
    "]\n",
    "format_guesses(bxor(base_key[:4], base_key[4:8]), [bg0b, bg1b, bg2b, bg3b], weight_progression=True, weight_key=wpkey)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bg0c, bg1c, bg2c, bg3c = [\n",
    "    CPA.single((lambda guess, txi: hw(txi[4+i] ^ txi[8+i] ^ guess)), attack_windows[i][1], attack_windows[i][2], attack_traces[i].textins, range(256), model=CPAModel.POS)\n",
    "    for i in range(4)\n",
    "]\n",
    "format_guesses(bxor(base_key[4:8], base_key[8:12]), [bg0c, bg1c, bg2c, bg3c], weight_progression=True, weight_key=wpkey)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bg0d, bg1d, bg2d, bg3d = [\n",
    "    CPA.single((lambda guess, txi: hw(txi[8+i] ^ txi[12+i] ^ guess)), attack_windows[i][1], attack_windows[i][2], attack_traces[i].textins, range(256), model=CPAModel.POS)\n",
    "    for i in range(4)\n",
    "]\n",
    "format_guesses(bxor(base_key[8:12], base_key[12:]), [bg0d, bg1d, bg2d, bg3d], weight_progression=True, weight_key=wpkey)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bg0e, bg1e, bg2e, bg3e = [\n",
    "    CPA.single((lambda guess, txi: hw(txi[12+i] ^ guess)), attack_windows[i][1], attack_windows[i][2], attack_traces[i].textins, range(256), model=CPAModel.POS)\n",
    "    for i in range(4)\n",
    "]\n",
    "format_guesses(base_key[12:], [bg0e, bg1e, bg2e, bg3e], weight_progression=True, weight_key=wpkey)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Crunching rows - combine individual row guesses into one\n",
    "**Optional**. In theory, the bg0a..bg3d should be enough to break the key, but this analysis takes into account bgXe results by combining all row results into a single weight (for each byte from `r4, r4^r5, r5^r6, r6^r7, r7`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# First, un-sort the guess lists and store them in files (5 files per row, 4 rows total)\n",
    "def unsort(fname, guess_list):\n",
    "    guess_clone = guess_list.copy()\n",
    "    guess_clone.sort(key=lambda x: x[0])\n",
    "    for i in range(len(guess_clone)):\n",
    "        # The crunching software adds weights, so transform correlations to additive form via log\n",
    "        var = guess_clone[i][1][0]\n",
    "        if var > 0:\n",
    "            guess_clone[i] = np.log(var)\n",
    "        else:\n",
    "            guess_clone[i] = -1e32\n",
    "    np_guess = np.asarray(guess_clone, dtype=np.float32)\n",
    "    np_guess.tofile(fname)\n",
    "\n",
    "unsort(f'{target_prefix}cpa-r0c0.bin', bg0a)\n",
    "unsort(f'{target_prefix}cpa-r1c0.bin', bg1a)\n",
    "unsort(f'{target_prefix}cpa-r2c0.bin', bg2a)\n",
    "unsort(f'{target_prefix}cpa-r3c0.bin', bg3a)\n",
    "\n",
    "unsort(f'{target_prefix}cpa-r0c0x1.bin', bg0b)\n",
    "unsort(f'{target_prefix}cpa-r1c0x1.bin', bg1b)\n",
    "unsort(f'{target_prefix}cpa-r2c0x1.bin', bg2b)\n",
    "unsort(f'{target_prefix}cpa-r3c0x1.bin', bg3b)\n",
    "\n",
    "unsort(f'{target_prefix}cpa-r0c1x2.bin', bg0c)\n",
    "unsort(f'{target_prefix}cpa-r1c1x2.bin', bg1c)\n",
    "unsort(f'{target_prefix}cpa-r2c1x2.bin', bg2c)\n",
    "unsort(f'{target_prefix}cpa-r3c1x2.bin', bg3c)\n",
    "\n",
    "unsort(f'{target_prefix}cpa-r0c2x3.bin', bg0d)\n",
    "unsort(f'{target_prefix}cpa-r1c2x3.bin', bg1d)\n",
    "unsort(f'{target_prefix}cpa-r2c2x3.bin', bg2d)\n",
    "unsort(f'{target_prefix}cpa-r3c2x3.bin', bg3d)\n",
    "\n",
    "unsort(f'{target_prefix}cpa-r0c3.bin', bg0e)\n",
    "unsort(f'{target_prefix}cpa-r1c3.bin', bg1e)\n",
    "unsort(f'{target_prefix}cpa-r2c3.bin', bg2e)\n",
    "unsort(f'{target_prefix}cpa-r3c3.bin', bg3e)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Perform horizontal combination (\"crunching\") - for each 4 byte combination compute sum of 5 weights and store them in a combined file\n",
    "# (NOTE: we call julia instead of using it through notebook or PyJulia to control multithreading params)\n",
    "JULIA_NUM_THREADS = 8\n",
    "!julia -t {JULIA_NUM_THREADS} ../pa-tools/scripts/xor-crunch-row.jl f32 {target_prefix}cpa-r0c0.bin {target_prefix}cpa-r0c0x1.bin {target_prefix}cpa-r0c1x2.bin {target_prefix}cpa-r0c2x3.bin {target_prefix}cpa-r0c3.bin {target_prefix}cpa-r0-out.bin\n",
    "!julia -t {JULIA_NUM_THREADS} ../pa-tools/scripts/xor-crunch-row.jl f32 {target_prefix}cpa-r1c0.bin {target_prefix}cpa-r1c0x1.bin {target_prefix}cpa-r1c1x2.bin {target_prefix}cpa-r1c2x3.bin {target_prefix}cpa-r1c3.bin {target_prefix}cpa-r1-out.bin\n",
    "!julia -t {JULIA_NUM_THREADS} ../pa-tools/scripts/xor-crunch-row.jl f32 {target_prefix}cpa-r2c0.bin {target_prefix}cpa-r2c0x1.bin {target_prefix}cpa-r2c1x2.bin {target_prefix}cpa-r2c2x3.bin {target_prefix}cpa-r2c3.bin {target_prefix}cpa-r2-out.bin\n",
    "!julia -t {JULIA_NUM_THREADS} ../pa-tools/scripts/xor-crunch-row.jl f32 {target_prefix}cpa-r3c0.bin {target_prefix}cpa-r3c0x1.bin {target_prefix}cpa-r3c1x2.bin {target_prefix}cpa-r3c2x3.bin {target_prefix}cpa-r3c3.bin {target_prefix}cpa-r3-out.bin"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute argsort for each combined row\n",
    "JULIA_NUM_THREADS = 8\n",
    "!julia -t {JULIA_NUM_THREADS} ../pa-tools/scripts/argsort.jl f32 {target_prefix}cpa-r0-out.bin {target_prefix}cpa-r0-rev.bin\n",
    "!julia -t {JULIA_NUM_THREADS} ../pa-tools/scripts/argsort.jl f32 {target_prefix}cpa-r1-out.bin {target_prefix}cpa-r1-rev.bin\n",
    "!julia -t {JULIA_NUM_THREADS} ../pa-tools/scripts/argsort.jl f32 {target_prefix}cpa-r2-out.bin {target_prefix}cpa-r2-rev.bin\n",
    "!julia -t {JULIA_NUM_THREADS} ../pa-tools/scripts/argsort.jl f32 {target_prefix}cpa-r3-out.bin {target_prefix}cpa-r3-rev.bin"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# (Optional) Find best candidate for each column within a row\n",
    "!julia ../pa-tools/scripts/argmax4d.jl f32 {target_prefix}cpa-r0-out.bin {target_prefix}cpa-r0-rev.bin {target_prefix}cpa-r0-max.bin\n",
    "!julia ../pa-tools/scripts/argmax4d.jl f32 {target_prefix}cpa-r1-out.bin {target_prefix}cpa-r1-rev.bin {target_prefix}cpa-r1-max.bin\n",
    "!julia ../pa-tools/scripts/argmax4d.jl f32 {target_prefix}cpa-r2-out.bin {target_prefix}cpa-r2-rev.bin {target_prefix}cpa-r2-max.bin\n",
    "!julia ../pa-tools/scripts/argmax4d.jl f32 {target_prefix}cpa-r3-out.bin {target_prefix}cpa-r3-rev.bin {target_prefix}cpa-r3-max.bin"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load row weights and argsort arrays (as mmaps)\n",
    "weights_r0 = np.memmap(f'{target_prefix}cpa-r0-out.bin', dtype=np.float32, mode='r')\n",
    "weights_r1 = np.memmap(f'{target_prefix}cpa-r1-out.bin', dtype=np.float32, mode='r')\n",
    "weights_r2 = np.memmap(f'{target_prefix}cpa-r2-out.bin', dtype=np.float32, mode='r')\n",
    "weights_r3 = np.memmap(f'{target_prefix}cpa-r3-out.bin', dtype=np.float32, mode='r')\n",
    "\n",
    "argsort_r0 = np.memmap(f'{target_prefix}cpa-r0-rev.bin', dtype=np.uint32, mode='r')\n",
    "argsort_r1 = np.memmap(f'{target_prefix}cpa-r1-rev.bin', dtype=np.uint32, mode='r')\n",
    "argsort_r2 = np.memmap(f'{target_prefix}cpa-r2-rev.bin', dtype=np.uint32, mode='r')\n",
    "argsort_r3 = np.memmap(f'{target_prefix}cpa-r3-rev.bin', dtype=np.uint32, mode='r')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "base_key_r0 = bytes(base_key[0::4])\n",
    "base_key_r1 = bytes(base_key[1::4])\n",
    "base_key_r2 = bytes(base_key[2::4])\n",
    "base_key_r3 = bytes(base_key[3::4])\n",
    "\n",
    "def find_rank(argsort, weights, base):\n",
    "    b32i = int.from_bytes(base, 'little')\n",
    "    print(f\"Base row key: {base.hex()} weight: {weights[b32i]} best weight: {weights[argsort[-1]]}\")\n",
    "    for i, k in enumerate(tqdm(argsort[::-1])):\n",
    "        if k == b32i:\n",
    "            return i, np.exp(weights[k])\n",
    "\n",
    "r0rank = find_rank(argsort_r0, weights_r0, base_key_r0)\n",
    "r1rank = find_rank(argsort_r1, weights_r1, base_key_r1)\n",
    "r2rank = find_rank(argsort_r2, weights_r2, base_key_r2)\n",
    "r3rank = find_rank(argsort_r3, weights_r3, base_key_r3)\n",
    "\n",
    "print(\"Total key rank:\")\n",
    "print(f\"Row 0 rank: {r0rank[0]}, weight: {r0rank[1]}\")\n",
    "print(f\"Row 1 rank: {r1rank[0]}, weight: {r1rank[1]}\")\n",
    "print(f\"Row 2 rank: {r2rank[0]}, weight: {r2rank[1]}\")\n",
    "print(f\"Row 3 rank: {r3rank[0]}, weight: {r3rank[1]}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Legal\n",
    "\n",
    "The code is published under MIT license (SPDX-License-Identifier: MIT), see [LICENSE](../LICENSE).\n",
    "The project has been cofounded by Polish National Centre for Research and Development (NCBR) under project \"Evaluation of Side Channel Attack Potential on Embedded Targets (ESCAPE)\", proj. sign. PL-TW/VII/5/2020."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "vscode": {
   "interpreter": {
    "hash": "ec13b33d7c27dd4ea377f0feb28c386c9b496b2a1406318fb53cf506254431ca"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
