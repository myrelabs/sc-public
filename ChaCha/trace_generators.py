# Copyright 2020 - 2022, EMCORE Sp. z o.o.
# SPDX-License-Identifier: MIT

# This file contains generators of (key, txi) pairs for trace gathering using
# `ScopeTarget` method `gather_for_generator`.

# NOTE: patools must be in sys.path

from patools.utils.misc import randbytes, bytewise_xor as bxor


challenge_key = bytes(32)


def set_challenge_key(key):
    """
    Set challenge key for challenge generators, e.g.:
     * gen_challenge
     * gen_uni_txi
    """
    global challenge_key
    challenge_key = key


def gen_uni_txi(n):
    """
    Generate `n` challenge traces with random input (txi) and constant key (challenge_key).

    :param n: number of pairs to generate
    :returns: **yields:** n (key, input) pairs
    """
    for _ in range(n):
        txi = randbytes(16)
        yield (challenge_key, txi)


def gen_uni_all(n):
    """
    Generate `n` traces with random input (txi) and keys.

    :param n: number of pairs to generate
    :returns: **yields:** n (key, input) pairs
    """
    for _ in range(n):
        key = randbytes(32)
        txi = randbytes(16)
        yield (key, txi)


def _input_gen(n, nonce, ctr_start=0):
    """
    Generate `n` IVs (for given nonce and ctr_start) used as txi.
    
    :param n: number of txis to generate
    :param nonce: 8 or 12 bytes of nonce
    :param ctr_start: initial counter value, 0 by default
    
    :returns: **yields:** n consecutive IVs.
    """
    ctr_bytes = 16 - len(nonce)
    for _ in range(n):
        yield ctr_start.to_bytes(ctr_bytes, 'little') + nonce
        ctr_start += 1


def gen_key_nonce_ctr(key, nonce, ctr_start=0):
    """
    Return a generator for `n` (key, txi) pairs using provided key
    and consecutive IVs derived from `nonce` and `ctr_start`.
    
    :param key: constant key for all traces
    :param nonce: 8 or 12 bytes of nonce
    :param ctr_start: initial counter value, 0 by default
    
    :returns: generator yielding n (key, input) pairs (with consecutive IVs).
    """
    def _gen(n):
        for txi in _input_gen(n, nonce, ctr_start):
            yield (key, txi)
    return _gen


def gen_challenge(nonce, ctr_start=0):
    """
    Return a generator for `n` (key, txi) pairs using challenge key
    and consecutive IVs derived from `nonce` and `ctr_start`.
    
    :param nonce: 8 or 12 bytes of nonce
    :param ctr_start: initial counter value, 0 by default
    
    :returns: generator yielding n (key, input) pairs (with consecutive IVs).
    """
    return gen_key_nonce_ctr(challenge_key, nonce, ctr_start)


def gen_tls_like(write_iv, seq_id, msg_blocks):
    """
    Return generator for `n` (key, txi) pairs using challenge key
    and IVs corresponding to a TLS stream for constant-size message blocks
    
    :param write_iv: 12 bytes of pseudorandom data, in TLS derived from MSK
    :param seq_id: 64-bit initial record sequence number (int)
    :param msg_blocks: number of traces (blocks) per record
    
    :returns: generator yielding n (key, input) pairs
    """
    def _gen(n):
        _seq_id = seq_id
        for _ in range(0, n, msg_blocks):
            nonce = bxor(_seq_id.to_bytes(12, 'big'), write_iv)
            yield from gen_key_nonce_ctr(challenge_key, nonce, 0)(msg_blocks)
            _seq_id += 1
    return _gen
