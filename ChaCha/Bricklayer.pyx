# Copyright 2020 - 2022, EMCORE Sp. z o.o.
# SPDX-License-Identifier: MIT

import random

import numpy as np
cimport numpy as np

ctypedef np.npy_uint32 uint32
ctypedef np.npy_uint64 uint64

DEF _hw_lookup_bits = 10
DEF _hw_lookup_limit = (1 << _hw_lookup_bits)
DEF _hw_lookup_mask = (_hw_lookup_limit - 1)
cdef uint32 HW[_hw_lookup_limit]

for x in range(_hw_lookup_limit):
    HW[x] = bin(x).count('1')

cdef uint32 hw(uint32 x):
    if x < _hw_lookup_limit:
        return HW[x]
    else:
        return HW[x & _hw_lookup_mask] + hw(x >> _hw_lookup_bits)

cdef uint64 rand_seed = random.getrandbits(64)
# Simple LCG (same consts as Newlib & Musl)
cdef uint64 rand_a = 6364136223846793005
cdef uint64 rand_b = 1
cdef uint32 getrandword():
    global rand_seed
    rand_seed *= rand_a
    rand_seed += rand_b
    return rand_seed >> 32

cdef uint32 ror(uint32 x, uint32 r):
    return (x >> r | (x << (32u - r))) & 0xFFFFFFFFu

cdef uint32 rol(uint32 x, uint32 r):
    return (x << r | (x >> (32u - r))) & 0xFFFFFFFFu

cdef uint32 _sel(uint32 x, uint32 start, uint32 count):
    return (x >> start) & ((1u << count) - 1u)


def masked_phi_key_split(n, k):
    mask_part = ((1 << n) - 1)
    p0 = k & mask_part
    k >>= n
    p1 = k & mask_part
    k >>= n
    p2 = k & mask_part
    kb = rol(p0, 7)
    kc = rol(p2, 20) | p1
    kb_mask = rol(mask_part, 7)
    kc_mask = rol(mask_part, 20) | mask_part
    return kb, kc, kb_mask, kc_mask


def masked_phi_key_grow(n, m, k, parts):
    # Grow all three components by m-n
    kb, kc, kb_mask, kc_mask = parts
    d = m - n
    mask_part = ((1 << d) - 1)
    p0 = k & mask_part
    k >>= d
    p1 = k & mask_part
    k >>= d
    p2 = k & mask_part
    kb |= rol(p0, (7 + n) & 31)
    kc |= rol(p2, (20 + n) & 31) | rol(p1, n)
    kb_mask |= rol(mask_part, (7 + n) & 31)
    kc_mask |= rol(mask_part, (20 + n) & 31) | rol(mask_part, n)
    return kb, kc, kb_mask, kc_mask


def masked_phi_key_grow2(n, m, k, parts):
    # Grow only p0 and p1 components by m-n
    kb, kc, kb_mask, kc_mask = parts
    d = m - n
    mask_part = ((1 << d) - 1)
    p0 = k & mask_part
    k >>= d
    p1 = k & mask_part
    kb |= rol(p0, (7 + n) & 31)
    kc |= rol(p1, n)
    kb_mask |= rol(mask_part, (7 + n) & 31)
    kc_mask |= rol(mask_part, n)
    return kb, kc, kb_mask, kc_mask


def masked_phi_key_grow3(n, m, k, parts):
    # Grow only p0 component by m-n
    kb, kc, kb_mask, kc_mask = parts
    d = m - n
    mask_part = ((1 << d) - 1)
    p0 = k & mask_part
    kb |= rol(p0, (7 + n) & 31)
    kb_mask |= rol(mask_part, (7 + n) & 31)
    return kb, kc, kb_mask, kc_mask


def random_masked_phi_3(block):
    def _select_hw(key_parts, txitxo):
        cdef uint32 kb, kc, kb_mask, kc_mask, kb_rand_mask, kc_rand_mask
        cdef uint32 b, c, d, nc, dt, bb, cc, kkb, kkc
        cdef uint32 kb_rand, kc_rand
        cdef uint32 hw_count, hw_total
        
        txi, txo = txitxo
        
        b = txo[1][(block + 1) & 3]
        c = txo[2][(block + 2) & 3]
        d = txo[3][(block + 3) & 3]
        nc = txi[(block + 3) & 3]
        dt = (d - nc) & 0xFFFFFFFFu

        kb, kc, kb_mask, kc_mask = key_parts
        kb_rand_mask = 0xFFFFFFFFu ^ kb_mask
        kc_rand_mask = 0xFFFFFFFFu ^ kc_mask
        hw_count = 2048u
        hw_total = 0u
        for _ in range(hw_count):
            kb_rand = getrandword() & kb_rand_mask
            kc_rand = getrandword() & kc_rand_mask
            kkb = kb | kb_rand
            kkc = kc | kc_rand
            bb = (b - kkb) & 0xFFFFFFFFu
            cc = (c - kkc) & 0xFFFFFFFFu
            bb = ror(bb, 7)
            bb ^= cc
            cc = (cc - dt) & 0xFFFFFFFFu
            bb = ror(bb, 12)
            bb ^= cc
            hw_total += hw(bb)
        return float(hw_total) / hw_count
    return _select_hw


def masked_phi_4(n, block):
    cdef uint32 mask_part = ((1 << n) - 1)
    # The extra_cbits invoke borrow in the "> 0.75 chance of carry" scenarios for kb and kc,
    # but only if the key mask doesn't reach their position
    cdef uint32 extra_cbit_kkb = (1u << 5u)
    cdef uint32 extra_cbit_kkc = (1u << 18u)
    def _select_hw(key_parts, txitxo):
        cdef uint32 kb, kc, kb_mask, kc_mask
        cdef uint32 b, c, d, nc, dt, bb, cc, kkb, kkc
        cdef uint32 tmp
        
        txi, txo = txitxo
        
        b = txo[1][(block + 1) & 3]
        c = txo[2][(block + 2) & 3]
        d = txo[3][(block + 3) & 3]
        nc = txi[(block + 3) & 3]
        dt = (d - nc) & 0xFFFFFFFFu

        kb, kc, kb_mask, kc_mask = key_parts
        kkb = kb | (extra_cbit_kkb & (extra_cbit_kkb ^ kb_mask))
        kkc = kc | (extra_cbit_kkc & (extra_cbit_kkc ^ kc_mask))
        bb = (b - kkb) & 0xFFFFFFFFu
        cc = (c - kkc) & 0xFFFFFFFFu
        bb = ror(bb, 7)
        bb ^= cc
        cc = (cc - dt) & 0xFFFFFFFFu
        bb = ror(bb, 12)
        bb ^= cc
        tmp = ror(bb, 20) & mask_part
        return hw(tmp)
    return _select_hw
